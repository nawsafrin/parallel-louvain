#!/bin/bash

LIMIT=1
#num_pro=40
num_pro=8
num_pro2=4
l1=0
fin=./sample/input/
fout=./sample/Output/
fout1=./sample/Plot/
fio=./sample/Output/full/
fc=-n2c
fx=-X
#f=example-l
#fw=example-lw
f=gt-l
fw=gt-lw
#f=p2p-G31-l
#fw=p2p-G31-lw
#f=ca-l
#fw=ca-lw
val=./Log/l-
ext1=.txt
ext2=.bin
ext3=.tree
q=-10000000000000000
rm -f $fout$f$fc$ext1

GYAN_INSTALL_PATH=/home/safrin/Downloads/mpi-tools-master/mpi_t/gyan

for ((l=0; l < LIMIT ; l++))  
do
	l1=`expr $l + 1`
	oldq=$q

	if (($l==1))
	then
#		num_pro=`expr $num_pro / 2`
		num_pro=4
#		if (($num_pro==0))
#		then
#			num_pro=1	
#		fi
	fi

	if (($l==2))
	then
#		num_pro=`expr $num_pro / 2`
		num_pro=2
#		if (($num_pro==0))
#		then
#			num_pro=1	
#		fi
	fi

	

	if (($l==0))
	then  
		./convert -i $fin$f$l$ext1 -o $fout$f$l -n $num_pro 
	#	LD_PRELOAD=/home/safrin/programs/valgrind-install/lib/valgrind/libmpiwrap-x86-linux.so

	#	LD_PRELOAD=$GYAN_INSTALL_PATH/libgyan.so 
		mpirun -np $num_pro varlist - ./louvain $fout$f$l -l -1  -i $fin$f$l1$ext1 -g $fio$f$l1$ext2 -x $fio$fw$l1$ext2  -f $fio$f$l$ext2 -u $fout$f$fc$ext1  -q id_qual > $fout$f$l$ext3
	else
		./convert -i $fin$f$l$ext1 -o $fout$f$l -w $fout$fw$l -n $num_pro 
#		LD_PRELOAD=/home/safrin/programs/valgrind-install/lib/valgrind/libmpiwrap-x86-linux.so
	#	LD_PRELOAD=$GYAN_INSTALL_PATH/libgyan.so 
		mpirun -np $num_pro  ./louvain $fout$f$l -w $fout$fw$l -l -1  -i $fin$f$l1$ext1 -g $fio$f$l1$ext2 -x $fio$fw$l1$ext2  -f $fio$f$l$ext2 -o $fio$fw$l$ext2 -u $fout$f$fc$ext1  -q id_qual > $fout$f$l$ext3
	fi
	input=$fout$f$l$ext3
	while IFS=: read -r var var2
	do
	  #echo $var
	  np=$var
	  q=$var2
	done < "$input"
	if [ "$(echo ${q}'<='${oldq} | bc -l)" = "1" ]
	then
		LIMIT=$l
	fi
	echo $np
	echo $q
	echo $l
	echo $LIMIT
#	if (($np<$num_pro2))
#	then
		#num_pro2=`expr $np / 2`
#		num_pro2=$np	
#	fi
#	echo $num_pro2

done

#./hierarchy $fout$f$fc$ext1 -l $LIMIT > $fout1$f$fc$LIMIT$ext1
#echo $fout1$f$fc$LIMIT$ext1
#./matrix $fout$f$fc$ext1 -l $LIMIT > $fout1$f$fx$LIMIT$ext1
#echo $fout1$f$fx$LIMIT$ext1


