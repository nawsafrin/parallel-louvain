// File: quality.h
// -- quality functions header file
//-----------------------------------------------------------------------------
// Community detection
// Based on the article "Fast unfolding of community hierarchies in large networks"
// Copyright (C) 2008 V. Blondel, J.-L. Guillaume, R. Lambiotte, E. Lefebvre
//
// And based on the article
// Copyright (C) 2013 R. Campigotto, P. Conde Céspedes, J.-L. Guillaume
//
// This file is part of Louvain algorithm.
//
// Louvain algorithm is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Louvain algorithm is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Louvain algorithm.  If not, see <http://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
// Author   : E. Lefebvre, adapted by J.-L. Guillaume and R. Campigotto
// Email    : jean-loup.guillaume@lip6.fr
// Location : Paris, France
// Time	    : July 2013
//-----------------------------------------------------------------------------
// see README.txt for more details
#ifndef QUALITY_H
#define QUALITY_H
#include <sstream>
#include "graph_binary.h"
using namespace std;
class Quality {
 public:

  Graph & g; // network to compute communities for
  int size; // nummber of nodes in the network and size of all vectors
  int g_size;	// total nodes in network
  string name;

  vector<long double> in1, tot1;

  vector<int> n2c; // community to which each node belongs
  vector<int> neigh_deg;	// stores each node's degree
  vector<int> level_comm;	// stores distinct community for each processor
  vector<int> comm_rcv; 	// stores how many community update belonging to different processor [size: # processes]
  vector<int> node_process_new;	// used to find out belonging processor of each node, contains starting node of each processor [size: # processes]
  vector <vector <int> > neigh_r;	//store neighbours of each node
  vector <vector <long double> > neigh_w_r;	//store neighbours' weights of each node
  //unordered_map<int, pair<vector<int>::iterator, vector<long double>::iterator> > um;
  vector <int> node_rem;
  vector <int> neigh_rem;	// containns best community for each node of each process
  vector <long double> neigh_w_rem;	// containns dnodecomm for each node of each process
  //Quality(Graph &gr, const std::string& n):g(gr),size(g.nb_nodes),name(n){}
  Quality(Graph &gr, const std::string& n):g(gr),size(g.nb_nodes),g_size(g.g_nb_nodes),name(n){}

  virtual ~Quality();

  // remove the node from its current community with which it has dnodecomm links
  virtual void remove(int node, int comm, long double dnodecomm)=0;

  // insert the node in comm with which it shares dnodecomm links
  virtual void insert(int node, int comm, long double dnodecomm)=0;
  virtual void insert_g(int node, int comm, long double dnodecomm, long double selfloop, long double wdeg)=0;

  // compute the gain of quality by adding node to comm
  virtual long double gain(int node, int comm, long double dnodecomm, long double w_degree)=0;

  // compute the quality of the current partition
  virtual long double quality()=0;
  virtual pair< long double, long double> quality_p()=0;
};
template<class T>
std::string to_string(T number){
  std::ostringstream oss;
  oss << number;
  return oss.str();
}
#endif // QUALITY_H
