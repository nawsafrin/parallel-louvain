1. (a) Conversion from a text format (each line contains a couple "src dest")
	./convert -i ./sample/example.txt -o ./sample/example
   (b) nodes can be renumbered from 0 to nb_nodes-1 using -r option
	./convert -i ./sample/example-r.txt -o ./sample/example-r -r ./sample/out-r
2. Computes communities with a specified quality function and displays hierarchical tree:
	 mpirun -np 4 ./louvain ./sample/example -l -1 -v -q id_qual > ./sample/Output/example.tree
