#include <iostream>
#include <vector>
#include <utility>
#include <string>
#include <unordered_map>
using namespace std; 
int main()
{
    // Create an unordered_map of three strings (that map to strings)
    int deg=5;
    vector <int> x, x1; //= new int[deg];
	vector <long double> y, y1;// = new long double[deg];
	for(int i=0;i<deg;i++){
	    //x[i]=i;
	    //y[i]=i*1.5;
	    //foo.push_back(make_pair(x[i],y[i]));
	    x.push_back(i);
	    y.push_back(i*1.5);
	}
	
	for(int i=0;i<3;i++){
	    //x[i]=i;
	    //y[i]=i*1.5;
	    //foo.push_back(make_pair(x[i],y[i]));
	    x1.push_back(i+5);
	    y1.push_back(i*5.5);
	}
	
	pair<vector<int>::iterator, vector<long double>::iterator> bar=make_pair(x.begin(),y.begin());
	pair<vector<int>::iterator, vector<long double>::iterator> foo=make_pair(x1.begin(),y1.begin());
	
	pair<vector<int>::iterator, vector<long double>::iterator> p=bar;
	
    for(int i=0;i<deg;i++){
        
        int neigh  = *(p.first+i);

		long double neigh_w = *(p.second+i);   
		cout << neigh << "\t"<< neigh_w << endl;
    }
	
/*	<pair  <int,long double>> foo;
	
	for(vector<pair <int,long double>>::const_iterator i = foo.begin(); i != foo.end(); ++i)

    {

        	cout << *i.first << "\t";

    }*/
    
    unordered_map< int, pair<vector<int>::iterator, vector<long double>::iterator> > um = {{1,bar},{2,foo}};
     // Iterate and print keys and values of unordered_map
    for( const auto& n : um ) {
        std::cout << "Key:[" << n.first << "]\n";
    }
    pair<vector<int>::iterator, vector<long double>::iterator> p1=um[2];
    for(int i=0;i<deg;i++){
        
        int neigh  = *(p1.first+i);

		long double neigh_w = *(p1.second+i);   
		cout << neigh << "\t"<< neigh_w << endl;
    }
    
    unordered_map<string,string> u = {
        {"RED","#FF0000"},
        {"GREEN","#00FF00"},
        {"BLUE","#0000FF"}
    };
 
    // Iterate and print keys and values of unordered_map
    for( const auto& n : u ) {
        std::cout << "Key:[" << n.first << "] Value:[" << n.second << "]\n";
    }
 
    // Add two new entries to the unordered_map
    u["BLACK"] = "#000000";
    u["WHITE"] = "#FFFFFF";
 
    // Output values by key
    cout << "The HEX of color RED is:[" << u["RED"] << "]\n";
    cout << "The HEX of color BLACK is:[" << u["BLACK"] << "]\n";
 
    return 0;
}
