#!/bin/bash

# Graph directory
GRAPH_DIR=../../../data/graphs

#GRAPHFILE=./sample/input/example-l1
#GRAPHFILE2=./sample/input/metis-output/example-l1

#GRAPHFILE=./sample/input/fb-l0
#GRAPHFILE2=./sample/input/metis-output/fb-l0

#GRAPHFILE=./sample/input/gt-l0
#GRAPHFILE2=./sample/input/metis-output/gt-l0


#GRAPHFILE=./sample/input/p2p-G04-l0
#GRAPHFILE2=./sample/input/metis-output/p2p-G04-l0

#GRAPHFILE=./sample/input/p2p-G08-l0
#GRAPHFILE2=./sample/input/metis-output/p2p-G08-l0

#GRAPHFILE=./sample/input/p2p-G09-l0
#GRAPHFILE2=./sample/input/metis-output/p2p-G09-l0






#GRAPHFILE=./sample/input/p2p-G25-l0
#GRAPHFILE2=./sample/input/metis-output/p2p-G25-l0

#GRAPHFILE=./sample/input/p2p-G30-l0
#GRAPHFILE2=./sample/input/metis-output/p2p-G30-l0


#GRAPHFILE=./sample/input/p2p-G31-l0
#GRAPHFILE2=./sample/input/metis-output/p2p-G31-l0

#GRAPHFILE=./sample/input/sd-l0
#GRAPHFILE2=./sample/input/metis-output/sd-l0

#GRAPHFILE=./sample/input/sd-l0
#GRAPHFILE2=./sample/input/metis-output/l1/two/sd-l0

#GRAPHFILE=./sample/input/lj-l0
#GRAPHFILE2=./sample/input/metis-output/lj-l0

#GRAPHFILE=./sample/input/yt-r-l0
#GRAPHFILE2=./sample/input/metis-output/yt-l0


#GRAPHFILE=./sample/input/dblp-r-l0
#GRAPHFILE2=./sample/input/metis-output/dblp-l0

GRAPHFILE=./sample/input/wv-l0
GRAPHFILE2=./sample/input/metis-output/wv-l0

#GRAPHFILE=./sample/input/amazon-r-l0
###GRAPHFILE2=./sample/input/metis-output/amazon-l0

#GRAPHFILE=./sample/input/lj-r-l0
#GRAPHFILE2=./sample/input/metis-output/lj-l0



#GRAPHFILE=./sample/input/rn-pa-r-l0
#GRAPHFILE2=./sample/input/metis-output/rn-pa-l0


num_pro=40

fin=./sample/input/
fin2=./sample/input/metis-output/
fout=./sample/Output/
fout1=./sample/Plot/
fio=./sample/Output/full/
fc=-n2c
fx=-X
f=example-l
fw=example-lw


# Use weighted graphs or not
USEWEIGHT=0

    if [ $USEWEIGHT -eq 0 ]; then
	WEIGHTFLAG=""
    else
	WEIGHTFLAG="-w"
    fi
    echo "./txt2metis.py ${WEIGHTFLAG} ${GRAPHFILE}.txt ${GRAPHFILE}.metis"
    ./txt2metis/./txt2metis.py ${WEIGHTFLAG} ${GRAPHFILE}.txt ${GRAPHFILE2}.metis
#
#for((num_pro_c=2; num_pro_c < 130 ; num_pro_c*=2))
#for((num_pro_c=110; num_pro_c < 210 ; num_pro_c+=10))
#do
 ######   num_pro=$num_pro_c
#    gpmetis ${GRAPHFILE2}.metis $num_pro 
#done	
#gpmetis ${GRAPHFILE2}.metis $num_pro -objtype=vol.

#	if (($l==0))
#	then  
#		./convert -i $fin$f$l$ext1 -o $fout$f$l -n $num_pro -m $fin2$f$l$ext4$num_pro

		
#	else
#		./convert -i $fin$f$l$ext1 -o $fout$f$l -w $fout$fw$l -n $num_pro -m $fin2$f$l$ext4$num_pro
#		./convert -i ./sample/input/example-l1.txt -o ./sample/Output/example-l1 -w ./sample/Output/example-lw1 -n 4 -m ./sample/input/metis-output/example-l1.metis.part.4
		
