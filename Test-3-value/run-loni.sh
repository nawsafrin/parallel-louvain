#!/bin/bash

LIMIT=10
num_pro=8
#num_pro=4
num_pro2=2
l1=0
fin=./sample/input/
fout=./sample/Output/
fout1=./sample/Plot/
fio=./sample/Output/full/
fc=-n2c
fx=-X
#f=example-l
#fw=example-lw
#20 20 4 2
#f=gt-l
#fw=gt-lw
# 20 8 8 4
#f=fb-l
#fw=fb-lw
# 16/8/4 4 4 4/2
#f=p2p-G08-l
#fw=p2p-G08-lw

#f=p2p-G09-l
#fw=p2p-G09-lw

f=p2p-G04-l
fw=p2p-G04-lw

#f=p2p-G25-l
#fw=p2p-G25-lw

#f=p2p-G30-l
#fw=p2p-G30-lw

#f=p2p-G31-l
#fw=p2p-G31-lw

# 20 4 2 2
#f=wv-l
#fw=wv-lw
# 8 4 4 2
#f=ca-l
#fw=ca-lw

#f=enron-l
#fw=enron-lw

#f=sd-l
#fw=sd-lw

#f=dblp-l
#fw=dblp-lw

ext1=.txt
ext2=.bin
ext3=.tree
q=-100000000000000000000000000
rm -f $fout$f$fc$ext1
for ((l=0; l < LIMIT ; l++))
do
        l1=`expr $l + 1`
        oldq=$q
#       num_pro=`expr $num_pro / 2`
#       if (($l%2==0))
        if (($l==1))
        then
                num_pro=4
#               num_pro=`expr $num_pro / 2`
        fi
        if (($l==2))
        then
#               num_pro=`expr $num_pro / 2`
                num_pro=4
#               if (($num_pro==0))
#               then
#                       num_pro=1       
#               fi
        fi
        if (($l==3))
        then
                num_pro=4
        fi
        if (($l==4))
        then
                num_pro=4
        fi
        if (($l==5))
        then
                num_pro=2
        fi

        if (($l==0))
        then
                ./convert -i $fin$f$l$ext1 -o $fout$f$l -n $num_pro

                mpirun -np $num_pro  ./louvain $fout$f$l -l -1  -i $fin$f$l1$ext1 -g $fio$f$l1$ext2 -x $fio$fw$l1$ext2  -f $fio$f$l$ext2 -u $fout$f$fc$ext1  -q id_qual > $fout$f$l$ext3
        else
                ./convert -i $fin$f$l$ext1 -o $fout$f$l -w $fout$fw$l -n $num_pro
                mpirun -np $num_pro  ./louvain $fout$f$l -w $fout$fw$l -l -1  -i $fin$f$l1$ext1 -g $fio$f$l1$ext2 -x $fio$fw$l1$ext2  -f $fio$f$l$ext2 -o $fio$fw$l$ext2 -u $fout$f$fc$ext1  -q id_qual > $fout$f$l$ext3
        fi
        input=$fout$f$l$ext3
        while IFS=: read -r var var2
        do
          #echo $var
          np=$var
          q=$var2
        done < "$input"
        if [ "$(echo ${q}'<'${oldq} | bc -l)" = "1" ]
        then
                LIMIT=$l

        elif [ "$(echo ${q}'=='${oldq} | bc -l)" = "1" ]
        then

                LIMIT=`expr $l - 1`
        fi
        echo $np
        echo $q
        echo $l
        echo $LIMIT
        if [ "$np" -lt "$num_pro" ]
        then
                num_pro=`expr $np / 2`
                #num_pro=$np    
        fi
#       echo $num_pro2

done

./hierarchy $fout$f$fc$ext1 -l $LIMIT > $fout1$f$fc$LIMIT$ext1
echo $fout1$f$fc$LIMIT$ext1
#./matrix $fout$f$fc$ext1 -l $LIMIT > $fout1$f$fx$LIMIT$ext1
#echo $fout1$f$fx$LIMIT$ext1
