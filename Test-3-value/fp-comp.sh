#!/bin/bash
oldq=-25.67
q=-125.67
if [ "$(echo ${q}'<'${oldq} | bc -l)" = "1" ]; then
   echo "yes"

elif [ "$(echo ${q}'=='${oldq} | bc -l)" = "1" ]; then
   echo "equal"
else
   echo "no"
fi
