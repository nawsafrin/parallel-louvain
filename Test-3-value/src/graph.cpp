// File: graph.cpp
// -- simple graph handling source file
//-----------------------------------------------------------------------------
// Community detection
// Based on the article "Fast unfolding of community hierarchies in large networks"
// Copyright (C) 2008 V. Blondel, J.-L. Guillaume, R. Lambiotte, E. Lefebvre
//
// And based on the article
// Copyright (C) 2013 R. Campigotto, P. Conde Céspedes, J.-L. Guillaume
//
// This file is part of Louvain algorithm.
//
// Louvain algorithm is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Louvain algorithm is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Louvain algorithm.  If not, see <http://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
// Author   : E. Lefebvre, adapted by J.-L. Guillaume and R. Campigotto
// Email    : jean-loup.guillaume@lip6.fr
// Location : Paris, France
// Time	    : July 2013
//-----------------------------------------------------------------------------
// see README.txt for more details


#include "graph.h"
//#include <iostream>
#include <sstream>
#include <string>

using namespace std;

string itos(int i) // convert int to string
{
    stringstream s;
    s << i;
    return s.str();
}


Graph::Graph(char *filename, int type) {
  ifstream finput;
  finput.open(filename,fstream::in);
  if (finput.is_open() != true) {
    cerr << "The file " << filename << " does not exist" << endl;
    exit(EXIT_FAILURE);
  }

  unsigned long long nb_links = 0ULL;

  while (!finput.eof()) {
    unsigned int src, dest;
    long double weight = 1.0L;

    if (type==WEIGHTED) {
      finput >> src >> dest >> weight;
	//cerr << src << " " << dest << " " << weight << endl;
    } else {
      finput >> src >> dest;
    }

    if (finput) {
      if (links.size()<=max(src,dest)+1) {
        links.resize(max(src,dest)+1);
      }
   /*   if (edge.size()<=max(src,dest)+1) {
        edge.resize(max(src,dest)+1);
      }
      if (weight.size()<=max(src,dest)+1) {
        weight.resize(max(src,dest)+1);
      }
      edge[src].push_back(dest);
      weight[src].push_back(weight);*/

			pair<int, long double> m  = make_pair(dest,weight);
			if(std::find(links[src].begin(), links[src].end(), m) != links[src].end()) {
			    /* v contains x */
				// do nothing
			} else {
			    /* v does not contain x */
				links[src].push_back(m);
			}

      //links[src].push_back(make_pair(dest,weight));
	//cerr << "1st edge" << src << " " << dest << " " << weight << endl;
      if (src!=dest){
		pair<int, long double> n  = make_pair(src,weight);
			if(std::find(links[dest].begin(), links[dest].end(), n) != links[dest].end()) {
			    /* v contains x */
				// do nothing
			} else {
			    /* v does not contain x */
				links[dest].push_back(n);
			}
        //links[dest].push_back(make_pair(src,weight));
	//cerr << "2nd edge" << dest << " " << src << " " << weight << endl;
        //edge[dest].push_back(src);
        //weight[dest].push_back(weight);

      }
      nb_links += 1ULL;
    }
  }
  cerr << "nb_links: " << nb_links  << endl;
/*  for(int i=0;i<links.size();i++)
  {
	for (int j=0;j<links[i].size();j++){cerr << i << " " << links[i][j].first << " " << links[i][j].second << endl; }
  }
*/
  finput.close();
}

void
Graph::renumber(int type, char *filename) {
  vector<int> linked(links.size(),-1);
  vector<int> renum(links.size(),-1);
  int nb = 0;

  ofstream foutput;
  foutput.open(filename, fstream::out);

  for (unsigned int i=0 ; i<links.size() ; i++) {
    if (links[i].size() > 0)
      linked[i] = 1;
  }

  for (unsigned int i=0 ; i<links.size() ; i++) {
    if (linked[i]==1) {
      renum[i] = nb++;
      foutput << i << " " << renum[i] << endl;
	cerr << "node-p: " << i << "node-n: " << renum[i] << endl;
    }
  }

  for (unsigned int i=0 ; i<links.size() ; i++) {
    if (linked[i]==1) {
      for (unsigned int j=0 ; j<links[i].size() ; j++) {
  	links[i][j].first = renum[links[i][j].first];
      }
	//cerr << "links[renum[i]]: " << links[renum[i]] << "links[" << i << "]: " << links[i] << endl;
	links[renum[i]] = links[i];

    }
  }
  links.resize(nb);
}

void
Graph::clean(int type) {
  for (unsigned int i=0 ; i<links.size() ; i++) {
    map<int, long double> m;
    map<int, long double>::iterator it;

    for (unsigned int j=0 ; j<links[i].size() ; j++) {
      it = m.find(links[i][j].first);
      if (it==m.end())
	m.insert(make_pair(links[i][j].first, links[i][j].second));
      else if (type==WEIGHTED)
      	it->second+=links[i][j].second;
    }

    vector<pair<int, long double> > v;
    for (it = m.begin() ; it!=m.end() ; it++)
      v.push_back(*it);
    links[i].clear();
    links[i] = v;
  }
}

void
Graph::display(int type) {
  for (unsigned int i=0 ; i<links.size() ; i++) {
    for (unsigned int j=0 ; j<links[i].size() ; j++) {
      int dest = links[i][j].first;
      long double weight = links[i][j].second;
      if (type==WEIGHTED)
	cout << "src: " << i << " dst: " << dest << " weight: " << weight << endl;
      else
	cout << "src: " << i << " dst: " << dest << endl;
    }
  }
}

void
Graph::display_binary(char *filename, char *filename_w, int type) {
  ofstream foutput;
  foutput.open(filename, fstream::out | fstream::binary);

  int s = links.size();
	 cout << " size: " << s << endl;
  // outputs number of nodes
  foutput.write((char *)(&s),sizeof(int));

  // outputs cumulative degree sequence
  unsigned long long tot = 0ULL;
  for (int i=0 ; i<s ; i++) {
    tot += (unsigned long long)links[i].size();
	cout << "Cum degree: " << tot << endl;
    foutput.write((char *)(&tot),sizeof(unsigned long long));
  }

  // outputs links
  for (int i=0 ; i<s ; i++) {
    for (unsigned int j=0 ; j<links[i].size() ; j++) {
      int dest = links[i][j].first;
	cout << "Dest: " << dest << endl;
      foutput.write((char *)(&dest),sizeof(int));
    }
  }
  foutput.close();

  // outputs weights in a separate file
  if (type==WEIGHTED) {
    ofstream foutput_w;
    foutput_w.open(filename_w,fstream::out | fstream::binary);
    for (int i=0 ; i<s ; i++) {
      for (unsigned int j=0 ; j<links[i].size() ; j++) {
	long double weight = links[i][j].second;
	foutput_w.write((char *)(&weight),sizeof(long double));
      }
    }
    foutput_w.close();
  }
}


void
Graph::display_binary_p(char *filename, char *filename_w, int type, int np) {
  ofstream foutput;
  ofstream foutput_w;
  int chunk_size, bonus, start, end;
  int array_size=links.size();;
  int N=np;
  chunk_size = array_size / N;
  bonus = array_size - chunk_size * N ; // i.e. remainder
  int z=0;
  char *p;
  //char *pp;
  const char *r="";
  std::string str;

  for (start = 0, end = chunk_size;start < array_size;start = end, end = start + chunk_size)
  {
	  p=filename;
	  //cout << "start: "<< start << " end: " << end << " bonus: " << bonus << endl;
	  if (bonus) {
	    end++;
	    bonus--;
	  }
	//  cout << " After adjusting: start: "<< start << " end: " << end << " bonus: " << bonus << endl;
	  /* do something with array slice over [start, end) interval */
          //temp =new int [end-start];
          int s=end-start;
	  int t=start;
	  str="";
	  str+= p;
	  str+= "-";
	  str+= itos(z);
	  str+= ".bin";
	  r=str.c_str();
	  //cout << "Starting from: " << t << " size: " << s << endl;
	  foutput.open(r, fstream::out | fstream::binary);

	  // outputs global size
  	  foutput.write((char *)(&array_size),sizeof(int));
		//cout << "Network size: " << array_size << endl;
	  // outputs starting node
  	  foutput.write((char *)(&t),sizeof(int));
		//cout << "Start: " << t << endl;
	  // outputs number of nodes
  	  foutput.write((char *)(&s),sizeof(int));
		//cout << "Node size: " << s << endl;
        //  for(int k=0;k<s;k++){

		// outputs cumulative degree sequence
		  unsigned long long tot = 0ULL;
		  for (int i=t ; i<t+s ; i++) {
		    tot += (unsigned long long)links[i].size();
			//cout << "Cum degree: " << tot << endl;
		    foutput.write((char *)(&tot),sizeof(unsigned long long));
		  }


		// outputs links
		  for (int i=t ; i<t+s ; i++) {
		    for (unsigned int j=0 ; j<links[i].size() ; j++) {
		      int dest = links[i][j].first;
			//cout << "Dest: " << dest << endl;
		      foutput.write((char *)(&dest),sizeof(int));
		    }
		    //t++;
		  }
		foutput.close();
		//cout << r << "closed" << endl;
		// outputs weights in a separate file
		  if (type==WEIGHTED) {
		    p=filename_w;
		    str="";
		    str+= p;
		    str+= "-";
		    str+= itos(z);
		    str+= ".bin";
		    r=str.c_str();
		    foutput_w.open(r,fstream::out | fstream::binary);
		    for (int i=t ; i<t+s ; i++) {
		      for (unsigned int j=0 ; j<links[i].size() ; j++) {
			long double weight = links[i][j].second;
			//cout << "Weight: " << weight << endl;
			foutput_w.write((char *)(&weight),sizeof(long double));
		      }
		    }
		    foutput_w.close();
		}

	 // }

	  //	cout << "ending........." << z << endl;
		z++;
  }

}
