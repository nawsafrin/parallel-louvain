#!/bin/bash
export OMP_NUM_THREADS=100
in=./sample_networks/p2p-G31-l0
#in=./sample_networks/yt-l0
out=./sample/ca
ext1=.txt
ext2=.bin
ext3=.tree
./convert -i $in$ext1 -o $in$ext2
#/home/safrin/programs/valgrind-install/bin/valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --show-reachable=yes -v --dsymutil=yes --log-file=$in-nc.vg.%p 
./louvain $in$ext2 -l -1 -v -q id_qual > $in$ext3
