// File: main_louvain.cpp
// -- community detection, sample main file
//-----------------------------------------------------------------------------
// Community detection
// Based on the article "Fast unfolding of community hierarchies in large networks"
// Copyright (C) 2008 V. Blondel, J.-L. Guillaume, R. Lambiotte, E. Lefebvre
//
// And based on the article
// Copyright (C) 2013 R. Campigotto, P. Conde Céspedes, J.-L. Guillaume
//
// This file is part of Louvain algorithm.
//
// Louvain algorithm is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Louvain algorithm is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with Louvain algorithm.  If not, see <http://www.gnu.org/licenses/>.
//-----------------------------------------------------------------------------
// Author   : E. Lefebvre, adapted by J.-L. Guillaume and R. Campigotto
// Email    : jean-loup.guillaume@lip6.fr
// Location : Paris, France
// Time	    : July 2013
//-----------------------------------------------------------------------------
// see README.txt for more details
#include <unistd.h>
#include <string.h>
#include <sstream>
#include <algorithm>
#include <mpi.h>
#include <sys/time.h>
//#include <omp.h>
//#include <iostream.h>
//#include <vector>
//#include <utility>
//#include <unordered_map>
#include "graph_binary.h"
#include "louvain.h"
#include "modularity.h"
/*
#include "zahn.h"
#include "owzad.h"
#include "goldberg.h"
#include "condora.h"
#include "devind.h"
#include "devuni.h"
#include "dp.h"
#include "shimalik.h"
#include "balmod.h"
*/
using namespace std;
char *processor_name;
time_t time_begin, time_end;

long long start_time, end_time;
int world_size;
int rank;

MPI_Request request;
MPI_Status status;

vector<int> s_l;
vector<int> r_l;

vector<int> node_list,  comm_rcv_2;
vector<int>comm_list;   // contains community requiring update from other processors
vector<int>comm_list_old;
vector<int>comm_list_new;

// input graph.bin file
char *filename = NULL;
// input weight.bin file
char *filename_w = NULL;
char *filename_part = NULL;
// input full_graph.bin file
char *filename_full = NULL;
// input full_weight.bin file
char *filename_full_w = NULL;
int type = UNWEIGHTED;
char *outfile = NULL;
// output for next level full_graph.txt in <src dest weight> format
char *infile = NULL;
// output for next level full_graph.bin
char *outfile_2= NULL;
// output for next level full_weight.bin
char *outfile_w= NULL;
// output for node vs community
char *n2cfile = NULL;

int level = 0;
int nb_pass = 0;
long double precision = 0.000001L;
int display_level = -2;
unsigned short id_qual = 0;
long double alpha = 0.5L;
int kmin = 1;
long double sum_se = 0.0L;
long double sum_sq = 0.0L;
long double max_w = 1.0L;
Quality *q;
//int num_threads=4;
bool verbose = false;
void
usage(char *prog_name, const char *more) {
  cerr << more;
  cerr << "usage: " << prog_name << " input_file [-q id_qual] [-f full_network_file] [-o full_network_weight_file] [-g outfile_2] [-x outfile_w] [-i infile] [-u n2c_file] [-m level] [-c alpha] [-k min] [-w weight_file] [-p part_file] [-e epsilon] [-l display_level] [-v] [-h]" << endl << endl;
  cerr << "input_file: file containing the graph to decompose in communities" << endl;
  cerr << "-q id\tthe quality function used to compute partition of the graph (modularity is chosen by default):" << endl << endl;
  cerr << "\tid = 0\t -> the classical Newman-Girvan criterion (also called \"Modularity\")" << endl;
/*  cerr << "\tid = 1\t -> the Zahn-Condorcet criterion" << endl;
  cerr << "\tid = 2\t -> the Owsinski-Zadrozny criterion (you should specify the value of the parameter with option -c)" << endl;
  cerr << "\tid = 3\t -> the Goldberg Density criterion" << endl;
  cerr << "\tid = 4\t -> the A-weighted Condorcet criterion" << endl;
  cerr << "\tid = 5\t -> the Deviation to Indetermination criterion" << endl;
  cerr << "\tid = 6\t -> the Deviation to Uniformity criterion" << endl;
  cerr << "\tid = 7\t -> the Profile Difference criterion" << endl;
  cerr << "\tid = 8\t -> the Shi-Malik criterion (you should specify the value of kappa_min with option -k)" << endl;
  cerr << "\tid = 9\t -> the Balanced Modularity criterion" << endl;
  cerr << endl;
  cerr << "-c al\tthe parameter for the Owsinski-Zadrozny quality function (between 0.0 and 1.0: 0.5 is chosen by default)" << endl;
  cerr << "-k min\tthe kappa_min value (for Shi-Malik quality function) (it must be > 0: 1 is chosen by default)" << endl;
  cerr << endl;
*/
  //cerr << "-t num_threads\tset num of processors" << endl;
  cerr << "-w file\tread the graph as a weighted one (weights are set to 1 otherwise)" << endl;
  cerr << "-p file\tstart the computation with a given partition instead of the trivial partition" << endl;
  cerr << "\tfile must contain lines \"node community\"" << endl;
  cerr << "-e eps\ta given pass stops when the quality is increased by less than epsilon" << endl;
  cerr << "-l k\tdisplays the graph of level k rather than the hierachical structure" << endl;
  cerr << "\tif k=-1 then displays the hierarchical structure rather than the graph at a given level" << endl;
  cerr << "-v\tverbose mode: gives computation time, information about the hierarchy and quality" << endl;
  cerr << "-h\tshow this usage message" << endl;
  exit(0);
}
void
parse_args(int argc, char **argv) {
  if (argc<2)
    usage(argv[0], "Bad arguments number\n");
  for (int i = 1; i < argc; i++) {
    if(argv[i][0] == '-') {
      switch(argv[i][1]) {
      case 'w':
	type = WEIGHTED;
        filename_w = argv[i+1];
	i++;
	break;
      case 'f':
	filename_full = argv[i+1];
	i++;
	break;
      case 'o':
	filename_full_w = argv[i+1];
	i++;
	break;
      case 'g':
	outfile_2 = argv[i+1];
	i++;
	break;
      case 'x':
	outfile_w = argv[i+1];
	i++;
	break;
      case 'i':
	infile = argv[i+1];
	i++;
	break;
      case 'u':
	n2cfile = argv[i+1];
	i++;
	break;
      case 'q':
	id_qual = (unsigned short)atoi(argv[i+1]);
	i++;
	break;
      case 'm':
	level = atoi(argv[i+1]);
	i++;
	break;
  /*    case 't':
        num_threads = atoi(argv[i + 1]);
        i++;
        break;
      case 'c':
	alpha = atof(argv[i+1]);
	i++;
	break;
      case 'k':
	kmin = atoi(argv[i+1]);
	i++;
	break;
*/
      case 'p':
        filename_part = argv[i+1];
	i++;
	break;
      case 'e':
	precision = atof(argv[i+1]);
	i++;
	break;
      case 'l':
	display_level = atoi(argv[i+1]);
	i++;
	break;
      case 'v':
	verbose = true;
	break;
      case 'h':
	usage(argv[0], "");
	break;
      default:
	usage(argv[0], "Unknown option\n");
      }
    } else {
      if (filename==NULL)
        filename = argv[i];
      else
        usage(argv[0], "More than one filename\n");
    }
  }
  if (filename == NULL)
    usage(argv[0], "No input file has been provided\n");
}
void
display_time(const char *str) {
  time_t rawtime;
  time ( &rawtime );
  cerr << str << ": " << ctime (&rawtime);
}
void
init_quality(Graph *g, unsigned short nbc) {
  if (nbc > 0)
    delete q;
  switch (id_qual) {
  case 0:
    q = new Modularity(*g);
    break;
/*  case 1:
    if (nbc == 0)
      max_w = g->max_weight();
    q = new Zahn(*g, max_w);
    break;
  case 2:
    if (nbc == 0)
      max_w = g->max_weight();
    if (alpha <= 0. || alpha >= 1.0)
      alpha = 0.5;
    q = new OwZad(*g, alpha, max_w);
    break;
  case 3:
    if (nbc == 0)
      max_w = g->max_weight();
    q = new Goldberg(*g, max_w);
    break;
  case 4:
    if (nbc == 0) {
      g->add_selfloops();
      sum_se = CondorA::graph_weighting(g);
    }
    q = new CondorA(*g, sum_se);
    break;
  case 5:
    q = new DevInd(*g);
    break;
  case 6:
    q = new DevUni(*g);
    break;
  case 7:
    if (nbc == 0) {
      max_w = g->max_weight();
      sum_sq = DP::graph_weighting(g);
    }
    q = new DP(*g, sum_sq, max_w);
    break;
  case 8:
    if (kmin < 1)
      kmin = 1;
    q = new ShiMalik(*g, kmin);
    break;
  case 9:
    if (nbc == 0)
      max_w = g->max_weight();
    q = new BalMod(*g, max_w);
    break;
 */ default:
    q = new Modularity(*g);
    break;
  }
}

void init()
{

  time(&time_begin);

  stringstream rankS;
  rankS << rank;
  //rankS << 1;
  string s(filename);

  string so(filename);
  s += "-" + rankS.str() + ".bin";

  //s += "-" + "1" + ".bin";
  so += "_out-" + rankS.str() + ".tree";
  unsigned short nb_calls = 0;


  char* tmp = new char[s.length() + 1];
  strcpy(tmp, s.c_str());



  outfile = new char[so.length() + 1];
  strcpy(outfile, so.c_str());
  if(type == WEIGHTED)
  {
	  string ss(filename_w);
	  ss += "-" + rankS.str() + ".bin";
	  filename_w =new char[ss.length() + 1];
	  strcpy(filename_w, ss.c_str());
  }
  if (verbose)
    display_time("Begin");
  //Graph g(filename, filename_w, type);
  Graph g(world_size, tmp, filename_w, type);



  init_quality(&g, nb_calls);
  g.display(outfile);
  //g.display();
  nb_calls++;

  if (verbose)
    cerr << endl << "Computation of communities with the " << q->name << " quality function" << endl << endl;


}

long long current_timestamp() {
    struct timeval te;
    gettimeofday(&te, NULL); // get current time
    long long milliseconds = te.tv_sec*1000LL + te.tv_usec/1000; // calculate milliseconds
    // printf("milliseconds: %lld\n", milliseconds);
    return milliseconds;
}



void exchange_starting_node(Louvain l)
{
	for(int i=0;i<world_size;i++)
	{
		int cn=(l.qual)->g.start;
		if(i!=rank)
		{



				MPI_Send(&cn, 1, MPI_INT, i, 1, MPI_COMM_WORLD);

			//s_l_size+=cn;
			//cout << "node " << rank << " sending " << cn << " to node " << i << " s_l_size: " << s_l_size << endl;

		}
	}
	for(int i=0;i<world_size;i++)
	{
		int r_s=0;
		if(i!=rank)
		{
			MPI_Recv(&r_s, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &status);
			(l.qual)->node_process_new[i]=r_s;
			//if(r_s==1)r_l.push_back(i);
			//r_l_size+=r_s;
			//cout << "Process " << rank << " receiving " << r_s << " from process " << i << endl;

		}
		else{(l.qual)->node_process_new[i]=(l.qual)->g.start;}
	}


//*************************************************************************************************
// Testing node_pro elements
/*	cout << "Process " << r << " node_pro elements: " <<  endl;
	for(int i=0;i<world_size;i++)
	{
		cout << (l.qual)->node_process_new[i] << "\t" ;
	}
	cout << endl;
*/
//************************************************************************************************************




}





int find_process (Louvain l, int node)
{
		int neigh_pro=-1;//=world_size-1;
		for(int k=0; k<world_size;k++)
		{
			//if(k!=rank)
			{
				if(node >= ((l.qual)->node_process_new[k]))
				{
					neigh_pro=k;
					//break;
				}
				//else neigh_pro=
			}
		}
		return neigh_pro;
}


void gather_neighbor_info(Louvain l)
{
  time_t time_begin_1, time_end_1;
  vector<int> receive_list;
  vector<int> c1;
  vector<long double> d1;
  long double sum;
  c1.resize((l.qual)->g_size);
  d1.resize((l.qual)->g_size);
	 while(!c1.empty()){c1.pop_back();}
	 while(!d1.empty()){d1.pop_back();}
  /*struct object {
	  int a;
	  int b;

	  int x[1000];
	  long double y[1000];
  };
  struct object myobject[(c.qual)->size];
  for(int i=0; i<(c.qual)->size;i++){
	int deg = ((c.qual)->g).nb_neighbors(i);


  }
  */
    long long start = current_timestamp();
  struct object {
	  int a;    //neighbour
	  //int b;    //edge-weight for a
	  int b;
	  int* x;   // neighbour-list of a
	  long double* y;   // neighbour-weight-list of a
  };
  struct object myobject, myobject2;
  MPI_Datatype newstructuretype;
  int structlen = 4;
  int blocklengths[structlen];
  MPI_Datatype types[structlen];
  MPI_Aint displacements[structlen];
  MPI_Datatype newstructuretype2;
  int structlen2 = 4;
  int blocklengths2[structlen2];
  MPI_Datatype types2[structlen2];
  MPI_Aint displacements2[structlen2];
  //int* neigh_deg=new int[(c.qual)->g_size];
  //int deg=1;
  for(int i=0; i<(l.qual)->size;i++){
	int deg = ((l.qual)->g).nb_neighbors(i);
    long double w_deg = ((l.qual)->g).weighted_degree(i);

	myobject.x = new int[deg];
	myobject.y = new long double[deg];
	// where are the components relative to the structure?
	blocklengths[0] = 1; types[0] = MPI_INT;
	displacements[0] = (size_t)&(myobject.a) - (size_t)&myobject;
	blocklengths[1] = 1; types[1] = MPI_INT;
	displacements[1] = (size_t)&(myobject.b) - (size_t)&myobject;
	blocklengths[2] = deg; types[2] = MPI_INT;
	displacements[2] = (size_t)&(myobject.x[0]) - (size_t)&myobject;
	blocklengths[3] = deg; types[3] = MPI_LONG_DOUBLE;
	displacements[3] = (size_t)&(myobject.y[0]) - (size_t)&myobject;
	MPI_Type_create_struct(structlen,blocklengths,displacements,types,&newstructuretype);
	MPI_Type_commit(&newstructuretype);
	{
	  MPI_Aint typesize;
	  MPI_Type_extent(newstructuretype,&typesize);
	  //if (myrank==0)
	  //printf("Type extent: %d bytes\n",typesize);
	}
	myobject.a = i+ ((l.qual)->g.start);

 	(l.qual)->neigh_deg[myobject.a]=deg;
 	(l.qual)->node_weight[myobject.a]=w_deg;

	//myobject.b = w_deg;
	myobject.b = (l.qual)->n2c[myobject.a];
	pair<vector<int>::iterator, vector<long double>::iterator> p = ((l.qual)->g).neighbors(i);
	vector<int> send_list;
	for(int j=0; j<deg;j++){
		int neigh  = *(p.first+j);
		long double neigh_w = (((l.qual)->g).weights.size()==0)?1.0L:*(p.second+j);
		c1.push_back(neigh);
	  	d1.push_back(neigh_w);
		int neigh_pro=find_process(l,neigh);//=world_size-1;
		/*for(int k=0; k<world_size;k++)
		{
			//if(k!=rank)
			{
				if(neigh >= ((c.qual)->node_process_new[k]))

				{
					neigh_pro=k;
					//break;
				}
				//else neigh_pro=

			}
		}*/
		//int neigh_comm = (c.qual)->n2c[neigh];
		//int
		//neigh_pro = neigh/(c.qual)->size;

		myobject.x[j]=neigh;
		myobject.y[j]=neigh_w;

		//cout  << "neigh: " << neigh << "neigh_pro: " << neigh_pro << endl;
		if(neigh_pro!=rank)
		{
			if(receive_list.size()==0)receive_list.push_back(neigh);
			if(std::find(receive_list.begin(), receive_list.end(), neigh) != receive_list.end()) {
			    /* v contains x */
				// do nothing
			} else {
			    /* v does not contain x */
				receive_list.push_back(neigh);
			}

			if(send_list.size()==0)send_list.push_back(neigh_pro);
			/*std::vector<int>::iterator it;
			for(int k=0; k<send_list.size();k++){
				if()

			}*/
			if(std::find(send_list.begin(), send_list.end(), neigh_pro) != send_list.end()) {
			    /* v contains x */
				// do nothing
			} else {
			    /* v does not contain x */
				send_list.push_back(neigh_pro);
			}
		}

	}
	(l.qual)->neigh_r[myobject.a]=c1;
	(l.qual)->neigh_w_r[myobject.a]=d1;

	  while(!c1.empty()){c1.pop_back();}
	  while(!d1.empty()){d1.pop_back();}
	//*************************************************************************************************************************
	// Testing Send_List Elements
	/*cerr << "Send_List Processess for node:" << i << endl;
	for(vector<int>::const_iterator z = send_list.begin(); z != send_list.end(); ++z)
    	{

        	//cerr << *z << "\t";
    	}

	cerr << endl << "Removing Send_List Elements:" << endl;
	*/
	//*****************************************************************************************************************************
	time(&time_begin_1);

	while(!send_list.empty()){
			int send_p=send_list.back();
			send_list.pop_back();
			//cerr << send_p << "\t";
			MPI_Send(&deg, 1, MPI_INT, send_p, 1,  MPI_COMM_WORLD);
			// I_Send Giving Error
			//MPI_Isend(&deg, 1, MPI_INT, send_p, 1, MPI_COMM_WORLD, &request);
//			Send & I_Send Both Working
			MPI_Send(&myobject,1,newstructuretype,send_p,0,MPI_COMM_WORLD);
            		//MPI_Isend(&myobject,1,newstructuretype,send_p,0,MPI_COMM_WORLD, &request);

	}
	MPI_Type_free(&newstructuretype);
	delete[]myobject.x;
	delete[]myobject.y;
      //  cerr << endl ;

  }
//********************************************************************************************************************
// Testing Receive List Elements
/*  cerr << end << "Receive_List Elements:" << endl;
  for(vector<int>::const_iterator i = receive_list.begin(); i != receive_list.end(); ++i)
  {

        	cerr << *i << "\t";
  }
  cerr << "receive_list.size: " <<  receive_list.size() << endl ;

*/
//*******************************************************************************************************************************

  pair<vector<int>::iterator, vector<long double>::iterator> n_w_pair;
//  unordered_map<int, pair<vector<int>::iterator, vector<long double>::iterator> > um;
////  cerr << endl << "Removing Receive_List Elements:" << endl;

  while(!receive_list.empty()){
    sum=0;
	int deg=1;
	//int receive_n = receive_list.back();
	//int receive_p = receive_n/(c.qual)->size;
	receive_list.pop_back();
	//cerr << "Node Popped: " << receive_n << "\t";
	MPI_Recv(&deg, 1, MPI_INT,  MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
	int rec_p=status.MPI_SOURCE;

	myobject2.x = new int[deg];
	myobject2.y = new long double[deg];

	  //c1.resize(deg);
	  //d1.resize(deg);
	// where are the components relative to the structure?
	blocklengths2[0] = 1; types2[0] = MPI_INT;
	displacements2[0] = (size_t)&(myobject2.a) - (size_t)&myobject2;
	blocklengths2[1] = 1; types2[1] = MPI_INT;
	displacements2[1] = (size_t)&(myobject2.b) - (size_t)&myobject2;
	blocklengths2[2] = deg; types2[2] = MPI_INT;
	displacements2[2] = (size_t)&(myobject2.x[0]) - (size_t)&myobject2;
	blocklengths2[3] = deg; types2[3] = MPI_LONG_DOUBLE;
	displacements2[3] = (size_t)&(myobject2.y[0]) - (size_t)&myobject2;
	MPI_Type_create_struct(structlen2,blocklengths2,displacements2,types2,&newstructuretype2);
	MPI_Type_commit(&newstructuretype2);
	{
	  MPI_Aint typesize2;
	  MPI_Type_extent(newstructuretype2,&typesize2);
	  //if (myrank==0)
	  //printf("Type extent: %d bytes\n",typesize2);
	}
	MPI_Recv(&myobject2,1,newstructuretype2,rec_p,0,MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	//MPI_Irecv(&myobject2,1,newstructuretype2,rec_p,0,MPI_COMM_WORLD, &request);
	  (l.qual)->neigh_deg[myobject2.a]=deg;
	  (l.qual)->n2c[myobject2.a]=myobject2.b;
	  (l.qual)->tot1[myobject2.a]=deg;

	  for(int i=0;i<deg;i++){
		c1.push_back(myobject2.x[i]);
	  	d1.push_back(myobject2.y[i]);
	  	sum+=myobject2.y[i];

	  }
/*	  cerr << endl << "c1 Elements:" << endl;
	  for(vector<int>::const_iterator i = c1.begin(); i != c1.end(); ++i)
	  {
			//cerr << *i << "\t";
	  }
//	  cerr << endl << "d1 Elements:" << endl;
	  for(vector<long double>::const_iterator i = d1.begin(); i != d1.end(); ++i)
	  {
			//cerr << *i << "\t";
	  }
//	  cerr << endl;*/
	  (l.qual)->neigh_r[myobject2.a]=c1;
	  (l.qual)->neigh_w_r[myobject2.a]=d1;
	  (l.qual)->node_weight[myobject2.a]=sum;
	  n_w_pair=make_pair(c1.begin(),d1.begin());
	  while(!c1.empty()){c1.pop_back();}
	  while(!d1.empty()){d1.pop_back();}
	  MPI_Type_free(&newstructuretype2);
//	  um[myobject2.a]=n_w_pair;
	 // printf("In Process: %d , After Receive from: %d : node : %d neigh[0]: %d \n",rank,rec_p,myobject2.a, myobject2.x[0]);
	  //printf("In Process: %d , After Receive from: %d : node_comm: %d \n",rank,rec_p,myobject2.b);
	  //printf("In Process: %d , After Receive from: %d : neigh[0]: %d \n",rank,rec_p,myobject2.x[0]);
	  //printf("In Process: %d , After Receive from: %d : neigh[1]: %d \n",rank,rec_p,myobject2.x[1]);
	  //printf("In Process: %d , After Receive from: %d : x[2]: %d \n",rank,rec_p,myobject2.x[2]);
	  //printf("In Process: %d , After Receive from: %d : y[0]: %Lf \n",rank,rec_p,myobject2.y[0]);
	   /* pair<vector<int>::iterator, vector<long double>::iterator> p1=um[myobject2.a];
	    for(int i=0;i<deg;i++){

			int neigh  = *(p1.first+i);
			long double neigh_w = *(p1.second+i);
			cout << "In Map: node: " << myobject2.a << "\t neigh: "<< neigh << "\t neigh_weight: "<< neigh_w << endl;
	    } */
        delete[]myobject2.x;
        delete[]myobject2.y;
  }
  //
  //
  /*
  	PMPI_Type_free(157): MPI_Type_free(datatype_p=0xbfc03964) failed
PMPI_Type_free(89).: Invalid datatype
Error comes

  */
 // cerr << "neigh_deg elements for process: " << rank << endl;
  for(int i=0;i<(l.qual)->g_size;i++){
  	//cerr << "node: " << i << "\t degree: "<< (l.qual)->neigh_deg[i] <<  "\t weighted-degree: "<< (l.qual)->node_weight[i]<< endl;
	((l.qual)->g).part_weight+=(l.qual)->node_weight[i];
  }

 // cerr << "g.part_weight: " << ((c.qual)->g).part_weight << endl ;

  time(&time_end_1);
  long long end=current_timestamp();
  if(rank==0)cerr << "Total duration in Send-Receive: (gathering neighbour info): " << (end - start) << " milisec" << endl;
 // if(rank==0)cerr << "Total duration in Send-Receive: (gathering neighbour info): " << (time_end_1-time_begin_1) << " sec" << endl;

  // **********************************************@@@@@@@@@@@@@@@@@@@@@@@@************************************
// Test Print Using Map
//Not Working: Giving Wrong Values
 /*
  MPI_Barrier( MPI_COMM_WORLD ) ;
  cout << endl << "for process:" << rank << "@@" << "Map Size:" << um.size() << "**" << endl;

  for (auto& x: um) {
    //cout << x.first << endl;
    pair<vector<int>::iterator, vector<long double>::iterator> p1=x.second;
    	for(int i=0;i<(c.qual)->neigh_deg[x.first];i++){


			int neigh  = *(p1.first+i);
			long double neigh_w = *(p1.second+i);
			cout << "Outside loop: In Map: node: " << x.first << "\t neigh: "<< neigh << "\t neigh_weight: "<< neigh_w << endl;
	    }
  }

	cout << "for process: " << rank << "" << endl;
*/
 /* for ( unordered_map<int, pair<vector<int>::iterator, vector<long double>::iterator> >:: const_iterator it = um.begin(); it != um.end(); ++it ){
	pair<vector<int>::iterator, vector<long double>::iterator> p1=um[it->first];
	int i=0;

	//for(int i=0;i<(l.qual)->neigh_deg[it->first];i++){

			int neigh1  = *(p1.first+i);
			long double neigh_w1 = *(p1.second+i);
			cout << "Outside loop: In Map: node: " << it->first << "\t neigh: "<< neigh1 << "\t neigh_weight: "<< neigh_w1 << endl;

	  //  }
    //std::cout << " " << it->first << ":" << it->second;
  }
	/*for(int j=0;j<(l.qual)->size;j++){
	    int node=j+ ((l.qual)->g.start);

  	    //pair<vector<int>::iterator, vector<long double>::iterator> p1=um.find(node);
	    for(int i=0;i<(l.qual)->neigh_deg[node];i++){

			int neigh  = *(p1.first+i);
			long double neigh_w = *(p1.second+i);

			cout << "In Map: node: " << node << "\t neigh: "<< neigh << "\t neigh_weight: "<< neigh_w << endl;
	    }
	} */
//********************************************************************************************************************************************
// Test Print o0f 1st Send-Receive Elements
//Working
/*
  MPI_Barrier( MPI_COMM_WORLD ) ;
  for(int ii=0; ii<(l.qual)->size;ii++){
	int deg1 = ((l.qual)->g).nb_neighbors(ii);
  	pair<vector<int>::iterator, vector<long double>::iterator> p2 = ((l.qual)->g).neighbors(ii);


	for(int jj=0; jj<deg1;jj++){
		int neigh1  = *(p2.first+jj);
		//long double neigh_w = (((c.qual)->g).weights.size()==0)?1.0L:*(p.second+j);
		//int neigh_comm = (l.qual)->n2c[neigh];

		//int neigh_pro1 = neigh1/(l.qual)->size;
		int neigh_pro1=find_process(l,neigh1);;//=world_size-1;

		int tt = ii + ((l.qual)->g).start;
		cout  << rank << "@@ node:" << tt << " neigh: " << neigh1 << " neigh_pro: " << neigh_pro1 << endl;


			vector<int> n_r=(l.qual)->neigh_r[neigh1];
			vector<long double> n_w_r=(l.qual)->neigh_w_r[neigh1];
			cout << endl << "n_r Size:" << n_r.size() << "&&n_r Elements:" << endl;
			  for(vector<int>::const_iterator z = n_r.begin(); z != n_r.end(); ++z)

			  {
					cout << *z << "\t";
			  }
			cout << endl << "Elements Ending" << endl;
			/*cout << endl << "n_w_r Elements:" << endl;

			  for(vector<long double>::const_iterator z1 = n_w_r.begin(); z1 != n_w_r.end(); ++z1)
			  {
					//cout << *z1 << "\t";
			  }
			//cout << endl ;*/

/*

	}
  }

*/




}

void compute_community(Louvain l)
{
    long double quality = (l.qual)->quality();
    //cerr <<  "Computation of modularity: " << quality << endl ;
    long double new_qual;
    bool improvement;
    if (verbose) {
      cerr << "level " << level << ":\n";
      display_time("  start computation");
      cerr << "  network size: "
	   << (l.qual)->g.nb_nodes << " nodes, "
	   << (l.qual)->g.nb_links << " links, "
	   << (l.qual)->g.total_weight << " weight" << endl;
    }


    improvement = l.one_level_p();
    new_qual = (l.qual)->quality();

   if (verbose)
      cerr << "  quality increased from " << quality << " to " << new_qual << endl;

    quality = new_qual;

    if (verbose)
      display_time("  end computation");


  }



void send_list_update(Louvain l)
{
    s_l.resize(world_size);
    while(!s_l.empty()){s_l.pop_back();}

	//int s_l_size=0;

	//updating comm_rcv array
	for(int i=0;i<((l.qual)->g).nb_nodes;i++)
	{
		//int node=i+((l.qual)->g).start;
		if((l.qual)->neigh_rem[i]>=0)
		{
			//cout << "node: " << node << " comm: " << (l.qual)->neigh_rem[i] << " dnodecomm: " << (l.qual)->neigh_w_rem[i] << "\t";
			int comm_pro  =find_process(l,(l.qual)->neigh_rem[i]);//(c.qual)->neigh_rem[i]/(c.qual)->size;
			((l.qual)->comm_rcv)[comm_pro]++;

		}
	}
	//cout << endl << "comm_rcv elements:" << endl;
	// updating send_list array
	for(int i=0;i<world_size;i++)
	{
		//cout << ((l.qual)->comm_rcv)[i] << "\t";
		if(((l.qual)->comm_rcv)[i]>0)
		{
			s_l.push_back(i);
			//s_l_size++;
		}
	}
	//cout << endl ;
		//****************************************************************************************************************************************
			//Testing neigh_rem & neigh_w_rem Elements:
/*			cout << endl << "neigh_rem Elements:" << (l.qual)->neigh_rem.size() << endl;

			  for(vector<int>::const_iterator z1 = (l.qual)->neigh_rem.begin(); z1 != (l.qual)->neigh_rem.end(); ++z1)
			  {
					cout << *z1 << "\t";
			  }

			//cout << endl ;
			cout << endl << "neigh_w_rem Elements:" << (l.qual)->neigh_w_rem.size() << endl;
			  for(vector<long double>::const_iterator z2 = (l.qual)->neigh_w_rem.begin(); z2 != (l.qual)->neigh_w_rem.end(); ++z2)

			  {
					cout << *z2 << "\t";
			  }

			cout << endl ;
*/



}


void receive_list_update(Louvain l)
{

    r_l.resize(world_size);
    while(!r_l.empty()){r_l.pop_back();}
	//int r_l_size=0;
	int r_s=0;
	for(int i=0;i<world_size;i++)
	{
		int cn=0;
		if(i!=rank)
		{
			if(((l.qual)->comm_rcv)[i]==0)
			{


				MPI_Send(&cn, 1, MPI_INT, i, 1, MPI_COMM_WORLD);

			}
			else
			{

				cn=1;
				MPI_Send(&cn, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
				//s_l_size+=cn;
				//cout << "node " << rank << " sending 1 to " << i << endl;
			}
			//s_l_size+=cn;
			//cout << "Processor " << rank << " sending " << cn << " to processor " << i  << endl;

		}
	}
	for(int i=0;i<world_size;i++)
	{
		if(i!=rank)
		{
			MPI_Recv(&r_s, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &status);
			if(r_s==1)r_l.push_back(i);
			//r_l_size+=r_s;
			//cout << "node " << rank << " receiving " << r_s << " from node " << i  << endl;

		}
	}
	//cout << "rank: " << rank << "r_l_size: " << r_l_size << " rvec size: " << r_l.size() << " s_l_size: " << s_l_size << " svec size: " << s_l.size() << endl;

}

void exchange_updated_community(Louvain l)
{
    time_t time_begin_1, time_end_1;
    long long start=current_timestamp();
    time(&time_begin_1);
	struct object_n {

	  int* x;   //node
	  int* z;   //comm
	  long double* y;   //dnodecomm
	  long double* u;   //selfloop(node)
	  long double* v;   //weighted_degree(node)
  };
  struct object_n myobject_n, myobject_n_2;
  MPI_Datatype newstructuretype3;
  int structlen3 = 5;
  int blocklengths3[structlen3];
  MPI_Datatype types3[structlen3];
  MPI_Aint displacements3[structlen3];

  MPI_Datatype newstructuretype4;
  int structlen4 = 5;
  int blocklengths4[structlen4];
  MPI_Datatype types4[structlen4];
  MPI_Aint displacements4[structlen4];
 // cout << "s_l size: " << s_l.size() << endl;
  while(!s_l.empty()){
			int send_p=s_l.back();
			s_l.pop_back();
			int dg = ((l.qual)->comm_rcv)[send_p];
			//cout << "Rank: " << rank << send_p << "\t" << dg << endl;
			MPI_Send(&dg, 1, MPI_INT, send_p, 1,  MPI_COMM_WORLD);
			//cout << "Process " << rank << " Sent: " << dg << " to pro " << send_p << endl;
			//MPI_Isend(&dg, 1, MPI_INT, send_p, 1, MPI_COMM_WORLD, &request);
	myobject_n.x = new int[dg];
	myobject_n.z = new int[dg];
	myobject_n.y = new long double[dg];
	myobject_n.u = new long double[dg];
	myobject_n.v = new long double[dg];
	// where are the components relative to the structure?

	blocklengths3[0] = dg; types3[0] = MPI_INT;
	displacements3[0] = (size_t)&(myobject_n.x[0]) - (size_t)&myobject_n;
	blocklengths3[1] = dg; types3[1] = MPI_INT;
	displacements3[1] = (size_t)&(myobject_n.z[0]) - (size_t)&myobject_n;
	blocklengths3[2] = dg; types3[2] = MPI_LONG_DOUBLE;
	displacements3[2] = (size_t)&(myobject_n.y[0]) - (size_t)&myobject_n;
	blocklengths3[3] = dg; types3[3] = MPI_LONG_DOUBLE;
	displacements3[3] = (size_t)&(myobject_n.u[0]) - (size_t)&myobject_n;
	blocklengths3[4] = dg; types3[4] = MPI_LONG_DOUBLE;
	displacements3[4] = (size_t)&(myobject_n.v[0]) - (size_t)&myobject_n;

	MPI_Type_create_struct(structlen3,blocklengths3,displacements3,types3,&newstructuretype3);
	MPI_Type_commit(&newstructuretype3);
	{
	  MPI_Aint typesize3;
	  MPI_Type_extent(newstructuretype3,&typesize3);
	  //if (myrank==0)
	  //printf("Type extent: %d bytes\n",typesize);
	}
	int j=0;
		for(int i=0;i<((l.qual)->g).nb_nodes;i++)
		{
			int node=i+((l.qual)->g).start;
			if((l.qual)->neigh_rem[i]>=0)
			{
				//cout << "node: " << node << " comm: " << (c.qual)->neigh_rem[i] << " dnodecomm: " << (c.qual)->neigh_w_rem[i] << "\t";
				int comm_pro  =find_process(l,(l.qual)->neigh_rem[i]);//(c.qual)->neigh_rem[i]/(c.qual)->size;
				if(comm_pro==send_p)
				{
					myobject_n.x[j]=node;
					myobject_n.z[j]=(l.qual)->neigh_rem[i];
					myobject_n.y[j]=(l.qual)->neigh_w_rem[i];
					myobject_n.u[j]=((l.qual)->g).nb_selfloops(i);
					//myobject_n.v[j]=((l.qual)->g).weighted_degree(i);

					myobject_n.v[j]=(l.qual)->node_weight[node];
					j++;
					//cerr << rank << "sending to" <<send_p << "$In EUC node:" << myobject_n.x[j] << "* SL:" << myobject_n.u[j] << "* WD:" << myobject_n.v[j] << "*" << endl;
				}

			}
		}
		j=0;
//**********************************************************************************************************************************************
// Test Print Send
		//	cout << "SEnding myobject elements: " << endl;
			/*for(int i=0;i<dg;i++){
			//cout << "Rank: " << rank << myobject_n.x[i] << "\t" << myobject_n.z[i] << "\t" <<  myobject_n.y[i] << endl;
			//(c.qual)->insert(myobject_n_2.x[i], myobject_n_2.z[i], myobject_n_2.y[i]);

			//c1.push_back(myobject2.x[i]);
		  	//d1.push_back(myobject2.y[i]);
		  	}*/
//***************************************************************************************************************************************************
			MPI_Send(&myobject_n,1,newstructuretype3,send_p,0,MPI_COMM_WORLD);
            		//MPI_Isend(&myobject_n,1,newstructuretype3,send_p,0,MPI_COMM_WORLD, &request);
            		MPI_Type_free(&newstructuretype3);

            		delete[]myobject_n.x;
                    delete[]myobject_n.y;
            		delete[]myobject_n.z;
                    delete[]myobject_n.u;
                    delete[]myobject_n.v;


	}
 //   cout << "After s_l size: " << s_l.size() << endl;
	//for (int k=0; k<r_l_size;k++){
//	cout << "r_l size: " << r_l.size() << endl;
	while(!r_l.empty()){
		int deg=0;
		//int receive_n = r_l.back();
		//int receive_p = receive_n/(c.qual)->size;
		r_l.pop_back();
		//cerr << "Node Popped: " << receive_n << "\t";
		MPI_Recv(&deg, 1, MPI_INT,  MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
		int rec_p=status.MPI_SOURCE;
		//cout << "Process " << rank << " Received: " << deg << " from pro " << rec_p << endl;
		myobject_n_2.x = new int[deg];
		myobject_n_2.z = new int[deg];
		myobject_n_2.y = new long double[deg];
		myobject_n_2.u = new long double[deg];
		myobject_n_2.v = new long double[deg];

		  //c1.resize(deg);
		  //d1.resize(deg);
		// where are the components relative to the structure?
		blocklengths4[0] = deg; types4[0] = MPI_INT;
		displacements4[0] = (size_t)&(myobject_n_2.x[0]) - (size_t)&myobject_n_2;
		blocklengths4[1] = deg; types4[1] = MPI_INT;
		displacements4[1] = (size_t)&(myobject_n_2.z[0]) - (size_t)&myobject_n_2;
		blocklengths4[2] = deg; types4[2] = MPI_LONG_DOUBLE;
		displacements4[2] = (size_t)&(myobject_n_2.y[0]) - (size_t)&myobject_n_2;
		blocklengths4[3] = deg; types4[3] = MPI_LONG_DOUBLE;
		displacements4[3] = (size_t)&(myobject_n_2.u[0]) - (size_t)&myobject_n_2;
		blocklengths4[4] = deg; types4[4] = MPI_LONG_DOUBLE;
		displacements4[4] = (size_t)&(myobject_n_2.v[0]) - (size_t)&myobject_n_2;

		MPI_Type_create_struct(structlen4,blocklengths4,displacements4,types4,&newstructuretype4);
		MPI_Type_commit(&newstructuretype4);
		{
		  MPI_Aint typesize4;
		  MPI_Type_extent(newstructuretype4,&typesize4);
		  //if (myrank==0)
		  //printf("Type extent: %d bytes\n",typesize2);
		}
		MPI_Recv(&myobject_n_2,1,newstructuretype4,rec_p,0,MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		//MPI_Irecv(&myobject2,1,newstructuretype2,rec_p,0,MPI_COMM_WORLD, &request);
		 // (c.qual)->neigh_deg[myobject2.a]=deg;
		//  (c.qual)->n2c[myobject2.a]=myobject2.b;
		//  (c.qual)->tot1[myobject2.a]=deg;
		  for(int i=0;i<deg;i++){
			//cout << "node:" << myobject_n_2.x[i] << "\t n2c" << myobject_n_2.z[i] << "\t dnc" <<  myobject_n_2.y[i] << endl;
			//cerr << rank << "received from" << rec_p <<endl;
			(l.qual)->insert_g(myobject_n_2.x[i], myobject_n_2.z[i], myobject_n_2.y[i], myobject_n_2.u[i], myobject_n_2.v[i]);
			//c1.push_back(myobject2.x[i]);
		  	//d1.push_back(myobject2.y[i]);
		  }
		  MPI_Type_free(&newstructuretype4);
                    delete[]myobject_n_2.x;
                    delete[]myobject_n_2.y;
            		delete[]myobject_n_2.z;
                    delete[]myobject_n_2.u;
                    delete[]myobject_n_2.v;

	}
//	cout << "After r_l size: " << r_l.size() << endl;
  //
  //
    time(&time_end_1);
    long long end=current_timestamp();
    if(rank==0)cerr << "Total duration in Send-Receive: (exchanging updated community): " << (end-start) << " milisec" << endl;
  //if(rank==0)cerr << "Total duration in Send-Receive: (exchanging updated community): " << (time_end_1-time_begin_1) << " sec" << endl;


}


void exchange_updated_community_2(Louvain l)
{
    time_t time_begin_1, time_end_1;
    long long start=current_timestamp();
    time(&time_begin_1);

 // cout << "s_l size: " << s_l.size() << endl;

    int*x;
    int*z;
    long double*y;
    long double*u;
    long double*v;


  while(!s_l.empty()){
			int send_p=s_l.back();
			s_l.pop_back();
			int dg = ((l.qual)->comm_rcv)[send_p];
			//cout << "Rank: " << rank << send_p << "\t" << dg << endl;
			MPI_Send(&dg, 1, MPI_INT, send_p, 0,  MPI_COMM_WORLD);
			//cout << "Process " << rank << " Sent: " << dg << " to pro " << send_p << endl;
			//MPI_Isend(&dg, 1, MPI_INT, send_p, 1, MPI_COMM_WORLD, &request);

 /*             int x [dg];   //node
              int z[dg];   //comm
              long double y[dg];   //dnodecomm
              long double u[dg];   //selfloop(node)
              long double v[dg];   //weighted_degree(node)
*/
            x = new int[dg];
            z = new int[dg];
            y = new long double[dg];
            u = new long double[dg];
            v = new long double[dg];

	int j=0;
		for(int i=0;i<((l.qual)->g).nb_nodes;i++)
		{
			int node=i+((l.qual)->g).start;
			if((l.qual)->neigh_rem[i]>=0)
			{
				//cout << "node: " << node << " comm: " << (c.qual)->neigh_rem[i] << " dnodecomm: " << (c.qual)->neigh_w_rem[i] << "\t";
				int comm_pro  =find_process(l,(l.qual)->neigh_rem[i]);//(c.qual)->neigh_rem[i]/(c.qual)->size;
				if(comm_pro==send_p)
				{
					x[j]=node;
					z[j]=(l.qual)->neigh_rem[i];
					y[j]=(l.qual)->neigh_w_rem[i];
                    u[j]=((l.qual)->g).nb_selfloops(i);
					//myobject_n.v[j]=((l.qual)->g).weighted_degree(i);

					v[j]=(l.qual)->node_weight[node];
					j++;
					//cerr << rank << "sending to" <<send_p << "$In EUC node:" << myobject_n.x[j] << "* SL:" << myobject_n.u[j] << "* WD:" << myobject_n.v[j] << "*" << endl;
				}

			}
		}
		j=0;
//**********************************************************************************************************************************************
// Test Print Send
		//	cout << "SEnding myobject elements: " << endl;
			/*for(int i=0;i<dg;i++){
			//cout << "Rank: " << rank << myobject_n.x[i] << "\t" << myobject_n.z[i] << "\t" <<  myobject_n.y[i] << endl;
			//(c.qual)->insert(myobject_n_2.x[i], myobject_n_2.z[i], myobject_n_2.y[i]);

			//c1.push_back(myobject2.x[i]);
		  	//d1.push_back(myobject2.y[i]);
		  	}*/
//***************************************************************************************************************************************************
			//MPI_Send(&myobject_n,1,newstructuretype3,send_p,0,MPI_COMM_WORLD);
			MPI_Send(x, dg, MPI_INT, send_p, 1, MPI_COMM_WORLD);
			MPI_Send(z, dg, MPI_INT, send_p, 2, MPI_COMM_WORLD);
			MPI_Send(y, dg, MPI_LONG_DOUBLE, send_p, 3, MPI_COMM_WORLD);
			MPI_Send(u, dg, MPI_LONG_DOUBLE, send_p, 4, MPI_COMM_WORLD);
			MPI_Send(v, dg, MPI_LONG_DOUBLE, send_p, 5, MPI_COMM_WORLD);

                    delete[]x;
                    delete[]y;
            		delete[]z;
                    delete[]u;
                    delete[]v;


	}
 //   cout << "After s_l size: " << s_l.size() << endl;
	//for (int k=0; k<r_l_size;k++){
//	cout << "r_l size: " << r_l.size() << endl;
    int*x2;
    int*z2;
    long double*y2;
    long double*u2;
    long double*v2;


	while(!r_l.empty()){
		int deg=0;
		//int receive_n = r_l.back();
		//int receive_p = receive_n/(c.qual)->size;
		r_l.pop_back();
		//cerr << "Node Popped: " << receive_n << "\t";
		MPI_Recv(&deg, 1, MPI_INT,  MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
		int rec_p=status.MPI_SOURCE;
		//cout << "Process " << rank << " Received: " << deg << " from pro " << rec_p << endl;
/*
              int x2 [deg];   //node
              int z2[deg];   //comm
              long double y2[deg];   //dnodecomm
              long double u2[deg];   //selfloop(node)
              long double v2[deg];   //weighted_degree(node)
*/

            x2 = new int[deg];
            z2 = new int[deg];
            y2 = new long double[deg];
            u2 = new long double[deg];
            v2 = new long double[deg];

		MPI_Recv(x2, deg, MPI_INT, rec_p, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(z2, deg, MPI_INT, rec_p, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(y2, deg, MPI_LONG_DOUBLE, rec_p, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(u2, deg, MPI_LONG_DOUBLE, rec_p, 4, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		MPI_Recv(v2, deg, MPI_LONG_DOUBLE, rec_p, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		//MPI_Recv(&myobject_n_2,1,newstructuretype4,rec_p,0,MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		//MPI_Irecv(&myobject2,1,newstructuretype2,rec_p,0,MPI_COMM_WORLD, &request);
		 // (c.qual)->neigh_deg[myobject2.a]=deg;
		//  (c.qual)->n2c[myobject2.a]=myobject2.b;
		//  (c.qual)->tot1[myobject2.a]=deg;
		  for(int i=0;i<deg;i++){
			//cout << "node:" << myobject_n_2.x[i] << "\t n2c" << myobject_n_2.z[i] << "\t dnc" <<  myobject_n_2.y[i] << endl;
			//cerr << rank << "received from" << rec_p <<endl;
			(l.qual)->insert_g(x2[i], z2[i],y2[i], u2[i], v2[i]);
			//c1.push_back(myobject2.x[i]);
		  	//d1.push_back(myobject2.y[i]);
		  }

                    delete[]x2;
                    delete[]y2;
            		delete[]z2;
                    delete[]u2;
                    delete[]v2;

	}
//	cout << "After r_l size: " << r_l.size() << endl;
  //
  //
    time(&time_end_1);
    long long end=current_timestamp();
    if(rank==0)cerr << "Total duration in Send-Receive: (exchanging updated community): " << (end-start) << " milisec" << endl;
  //if(rank==0)cerr << "Total duration in Send-Receive: (exchanging updated community): " << (time_end_1-time_begin_1) << " sec" << endl;


}

void resolve_duality(Louvain l)
{

	//vector<vector<int>>comm_list;
	comm_rcv_2.resize(world_size,0);
/*	cout <<  " After resizing comm_rcv elements:" << endl;
	for(int i=0;i<world_size;i++)

	{
		cout << comm_rcv_2[i] << "\t";

	}

	cout << endl;
*/
/*    comm_list.resize( ((l.qual)->g).nb_nodes );
    while(!comm_list.empty()){comm_list.pop_back();}
    node_list.resize( ((l.qual)->g).nb_nodes );
    while(!node_list.empty()){node_list.pop_back();}
*/
	for(int i=0;i<((l.qual)->g).nb_nodes;i++)
	{
		int node=i+((l.qual)->g).start;
		int comm=(l.qual)->n2c[node];
		int j=0;
		if(comm<node)
		{


			//cout << "node: " << node << " comm: " << (c.qual)->neigh_rem[i] << " dnodecomm: " << (c.qual)->neigh_w_rem[i] << "\t";
			int comm_pro  =find_process(l,comm);//(c.qual)->neigh_rem[i]/(c.qual)->size;
			if(comm_pro==rank)
			{
				int m=(l.qual)->n2c[comm];
				(l.qual)->n2c[node]=m;
				//cout << "node: " << node << " comm: " << (l.qual)->n2c[node] << endl;
			}
			else
			{
				comm_rcv_2[comm_pro]++;
				node_list.push_back(node);
				if(comm_list.size()==0){comm_list.push_back(comm);j++;}
				if(std::find(comm_list.begin(), comm_list.end(), comm) != comm_list.end()) {
				    /* v contains x */
					// do nothing
				} else {
				    /* v does not contain x */
					comm_list.push_back(comm);
				}
			}


		}
	}
	//cerr << "Rank: " << rank << " size of community list requiring update: " << comm_list.size() << endl;

}

void resolve_duality_2(Louvain l){

    for (int i=0;i<world_size;i++){
        int comm_list_2[comm_list.size()];
        int comm_list_size=comm_list.size();
        int j=0;
        for( vector<int>::iterator iter = comm_list.begin(); iter != comm_list.end(); ++iter )
			    {
                    int comm=*iter;
                    comm_list_2[j]=comm;
                    k++;
                }
        if(i==rank)MPI_Bcast(&comm_list_size, 1, MPI_INT, i, MPI_COMM_WORLD);
        if(i!=rank)int comm_list_2[comm_list_size];
        if(i==rank)MPI_Bcast(comm_list_2, comm_list.size(), MPI_INT, i, MPI_COMM_WORLD);
        if(i!=rank){
            for(int k=0;k<comm_list_size;k++){
                int node = comm_list_2[k];
                int find_pro=find_process(l,node);
                if(find_pro==rank){
                        if(comm_list_old.size()==0){comm_list_old.push_back(node)}
                        if(std::find(comm_list_old.begin(), comm_list_old.end(), node) != comm_list_old.end()) {
                            /* v contains x */
                            // do nothing
                        } else {
                            /* v does not contain x */
                            comm_list_old.push_back(node);
                        }
                        for( vector<int>::iterator iter = comm_list_old.begin(); iter != comm_list_old.end(); ++iter )
                        {
                            int comm=*iter;
                            comm_list_new.push_back((l.qual)->n2c[comm]);
                            k++;
                        }




                }



            }



        }



    }







}






void send_list2_update()
{
    while(!s_l.empty()){s_l.pop_back();}
	for(int i=0;i<world_size;i++)
	{
		//cout << comm_rcv_2[i] << "\t";
		if(comm_rcv_2[i]>0)
		{
			s_l.push_back(i);


		}
	}

}

void receive_list_update()
{
    for(int i=0;i<world_size;i++)
	{
		int cn=0;
		if(i!=rank)
		{
			if(comm_rcv_2[i]==0)
			{


				MPI_Send(&cn, 1, MPI_INT, i, 1, MPI_COMM_WORLD);

			}
			else
			{

				cn=1;
				MPI_Send(&cn, 1, MPI_INT, i, 1, MPI_COMM_WORLD);
				//s_l_size+=cn;
				//cout << "node " << rank << " sending 1 to " << i << endl;
			}
			//s_l_size+=cn;
			//cout << "node " << rank << " sending " << cn << " to node " << i << " s_l_size: " << s_l_size << endl;

		}
	}
	while(!r_l.empty()){r_l.pop_back();}
	int r_s=0;
	for(int i=0;i<world_size;i++)
	{
		if(i!=rank)
		{
			MPI_Recv(&r_s, 1, MPI_INT, i, 1, MPI_COMM_WORLD, &status);
			if(r_s==1)r_l.push_back(i);

			//cout << "node " << rank << " receiving " << r_s << " from node " << i << "r_l_size: " << r_l_size << endl;

		}
	}

}
//send old community to the processor having community and get new community from the processor
void exchange_duality_resolved_community(Louvain l)
{
    time_t time_begin_1, time_end_1;
    time(&time_begin_1);

   // Structure not needed. Can receive the community only, receiving node redundant
 /* struct object_nnnn {

	  int* x;
	  int* y;

  };
  struct object_nnnn myobject_nnnn, myobject_nnnn_2;
  MPI_Datatype newstructuretype9;
  int structlen9 = 2;
  int blocklengths9[structlen9];
  MPI_Datatype types9[structlen9];
  MPI_Aint displacements9[structlen9];
  MPI_Datatype newstructuretype10;
  int structlen10 = 2;
  int blocklengths10[structlen10];
  MPI_Datatype types10[structlen10];
  MPI_Aint displacements10[structlen10];*/
// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	// Send
	vector<int> send_comm;
	long long start=current_timestamp();
//    send_comm.resize(world_size);
 //   while(!send_comm.empty()){send_comm.pop_back();}
	while(!s_l.empty()){
			int send_p=s_l.back();
			s_l.pop_back();

			//send_comm.resize(dg);
			//int j=0;

			    for( vector<int>::iterator iter = comm_list.begin(); iter != comm_list.end(); ++iter )
			    {
                    int comm=*iter;
                    int comm_pro=find_process(l,comm);
                    //cout << "send_p: " << send_p << " comm: " << comm << " comm_pro: " << comm_pro << endl;
                    if( comm_pro == send_p )
                    {
                        send_comm.push_back(comm);

                    }
			    }
			int dg = send_comm.size();

            int old_comm[dg];
            int new_comm[dg];
			for(int i=0;i<dg;i++)
			{

				old_comm[i]=send_comm[i];
				//cout << snd_comm[i] << "\t";
			}

			//cout << endl ;

		//**************************************Test Print **************************************************************
				/*
			cout << comm_list.size() << " comm_list elements: After Erase " << endl;
			for(vector<int>::const_iterator z2 = comm_list.begin(); z2 != comm_list.end(); ++z2)
			{
				cout << *z2 << "\t";
			}
			cout << endl ;
			cout << send_comm.size() << " send_comm elements:" << endl;
			for(vector<int>::const_iterator z2 = send_comm.begin(); z2 != send_comm.end(); ++z2)
			{
				cout << *z2 << "\t";
			}
			cout << endl ;
			*/
		//**************************************Test Print **************************************************************
			while(!send_comm.empty()){send_comm.pop_back();}
			//cout << "Rank: " << rank << send_p << "\t" << dg << endl;
			//MPI_Send(&dg, 1, MPI_INT, send_p, 1,  MPI_COMM_WORLD);
			//MPI_Send(&myobject_nnn,1,newstructuretype7,send_p,0,MPI_COMM_WORLD);

			MPI_Send(old_comm, dg, MPI_INT, send_p, 3, MPI_COMM_WORLD);
			//MPI_Send(&s_comm, dg, MPI_INT, send_p, 1, MPI_COMM_WORLD);
			//MPI_Isend(&send_comm, dg, MPI_INT, send_p, 1, MPI_COMM_WORLD, &request);
			//cout << "Sending done" << endl;
			//MPI_Type_free(&newstructuretype7);
		/*	myobject_nnnn.x = new int[dg];
			myobject_nnnn.y = new int[dg];
			blocklengths9[0] = dg; types9[0] = MPI_INT;
			displacements9[0] = (size_t)&(myobject_nnnn.x[0]) - (size_t)&myobject_nnnn;
			blocklengths9[1] = dg; types9[1] = MPI_INT;
			displacements9[1] = (size_t)&(myobject_nnnn.y[0]) - (size_t)&myobject_nnnn;

			MPI_Type_create_struct(structlen9,blocklengths9,displacements9,types9,&newstructuretype9);
			MPI_Type_commit(&newstructuretype9);
			{
			  MPI_Aint typesize9;
			  MPI_Type_extent(newstructuretype9,&typesize9);
			  //if (myrank==0)
			  //printf("Type extent: %d bytes\n",typesize);
			}
			MPI_Recv(&myobject_nnnn,1,newstructuretype9,send_p,2,MPI_COMM_WORLD, MPI_STATUS_IGNORE);*/
			MPI_Recv(new_comm, dg, MPI_INT, send_p, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

//*******************************************************************************************************************************
			/*cout  << " After Receive myobject x-y elements:" << endl;
			for(int zzmm=0;zzmm<dg;zzmm++)
			{cout << "x: " << myobject_nnnn.x[zzmm] << " y: " << myobject_nnnn.y[zzmm] << endl;}
				cout << endl ;
			*/
//*************************************************************************************************************************
			for( vector<int>::iterator iter = node_list.begin(); iter != node_list.end(); ++iter )
            {
				int node=*iter;
				int comm=(l.qual)->n2c[node];
				for(int k=0;k<dg;k++){
					if(old_comm[k] ==comm)
					{
						(l.qual)->n2c[node]=new_comm[k];
						//node_list.erase( iter );
					}

				}

			 }


		/*	MPI_Type_free(&newstructuretype9);
			delete[]myobject_nnnn.x;
            delete[]myobject_nnnn.y;*/


	}
	// Receive
	while(!r_l.empty()){
		int deg=0;

		r_l.pop_back();

//???????????????????????????????????????????????????????????????????????????????????????????????
    // Receiving n2c array from all
			MPI_Probe(MPI_ANY_SOURCE, 3, MPI_COMM_WORLD, &status);
            int rec_p=status.MPI_SOURCE;
			int number_amount;
		        // When probe returns, the status object has the size and other
		        // attributes of the incoming message. Get the message size
		        MPI_Get_count(&status, MPI_INT, &number_amount);

		        // Allocate a buffer to hold the incoming numbers
		        int* number_buf = (int*)malloc(sizeof(int) * number_amount);

		        // Now receive the message with the allocated buffer
		        MPI_Recv(number_buf, number_amount, MPI_INT, rec_p, 3, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

//??????????????????????????????????????????????????????????????????????????????????????????????????????
            deg=number_amount;
	/*		myobject_nnnn_2.x = new int[deg];
			myobject_nnnn_2.y = new int[deg];
			blocklengths10[0] = deg; types10[0] = MPI_INT;
			displacements10[0] = (size_t)&(myobject_nnnn_2.x[0]) - (size_t)&myobject_nnnn_2;
			blocklengths10[1] = deg; types10[1] = MPI_INT;
			displacements10[1] = (size_t)&(myobject_nnnn_2.y[0]) - (size_t)&myobject_nnnn_2;

			MPI_Type_create_struct(structlen10,blocklengths10,displacements10,types10,&newstructuretype10);
			MPI_Type_commit(&newstructuretype10);
			{
			  MPI_Aint typesize10;
			  MPI_Type_extent(newstructuretype10,&typesize10);
			  //if (myrank==0)
			  //printf("Type extent: %d bytes\n",typesize);
			}*/

//???????????????????????????????????????????????????????????????????????????????????????????????????????
	//	cerr << rank << "\t" << "n2c elements: " << endl;
            int send_new_comm[deg];
			for(int i=0;i< number_amount; i++){

				int comm=number_buf[i];
				//myobject_nnnn_2.x[i]= comm;
                send_new_comm[i]=(l.qual)->n2c[comm];
			//	cerr <<   " x: " << comm <<   " y: " << myobject_nnnn_2.y[i] << endl;
			}

        free(number_buf);

//????????????????????????????????????????????????????????????????????????????????????????????????????????
    MPI_Send(send_new_comm, deg, MPI_INT, rec_p, 2, MPI_COMM_WORLD);

    /*    MPI_Send(send_new_comm,deg,newstructuretype10,rec_p,2,MPI_COMM_WORLD);
        MPI_Type_free(&newstructuretype10);
        delete[]myobject_nnnn_2.x;
        delete[]myobject_nnnn_2.y;
*/
/*		cout << r_l.size() << " After Pop r_l elements:" << endl;
	for(vector<int>::const_iterator z2 = r_l.begin(); z2 != r_l.end(); ++z2)
	{
		cout << *z2 << "\t";
	}
	cout << endl ;
*/

	}
	//cout << "Outside While " << endl;

//MPI_Type_free(&newstructuretype9);
//MPI_Type_free(&newstructuretype10);

    time(&time_end_1);
    long long end=current_timestamp();
    if(rank%10==0)cerr << "Total duration in Send-Receive: (exchanging duality resolved community) " << (end-start) << " milisec" << endl;
 //   if(rank==0)cerr << "Total duration in Send-Receive: (exchanging duality resolved community) " << (time_end_1-time_begin_1) << " sec" << endl;



}

void find_unique_communities(Louvain l)
{
    while(!(l.qual)->level_comm.empty()){(l.qual)->level_comm.pop_back();}
    for(int i=0;i<(l.qual)->size;i++)
    {
        int node=i+((l.qual)->g).start;
        int comm=(l.qual)->n2c[node];
        if((l.qual)->level_comm.size()==0){(l.qual)->level_comm.push_back(comm);}
        if(std::find((l.qual)->level_comm.begin(), (l.qual)->level_comm.end(), comm) != (l.qual)->level_comm.end()) {
                        /* v contains x */
                        // do nothing
        } else {
            /* v does not contain x */
            (l.qual)->level_comm.push_back(comm);
        }
    }
     //   MPI_Barrier( MPI_COMM_WORLD ) ;   //barrier for test print
    //************************************************************************************************************************************
    /*	cout << (l.qual)->level_comm.size() << " (l.qual)->level_comm elements:" << endl;
        for(vector<int>::const_iterator z2 = (l.qual)->level_comm.begin(); z2 != (l.qual)->level_comm.end(); ++z2)
        {
            cout << *z2 << "\t";
        }
        cout << endl ;	*/
    //************************************************************************************************************************************




}


void renumber_communities(Louvain l)
{
    pair<long double, long double> qp = (l.qual)->quality_p();
	long double ww=(l.qual)->g.total_weight;
	long double ww2=(l.qual)->g.part_weight;
	long double in_l=qp.first/ww;
	long double in_l2=qp.first/ww2;

	long double tot_l=(qp.second*qp.second)/(ww*ww);
	long double tot_l2=(qp.second*qp.second)/(ww2*ww2);

	//cout << "in value and tot values: " << in_l - tot_l <<  "\t part: " << in_l2 - tot_l2 << endl;

  	struct object_nn {

		  int* x;   //community
		  long double y;    //tot value
		  long double z;    //in value
		  long double w;    //weight
	};
	struct object_nn myobject_nn;

	  MPI_Datatype newstructuretype5;
	  int structlen5 = 4;
	  int blocklengths5[structlen5];
	  MPI_Datatype types5[structlen5];
	  MPI_Aint displacements5[structlen5];
      long long start=current_timestamp();
	  if(rank!=0){



	  //MPI_Datatype newstructuretype6;
	  //int structlen6 = 3;
	  //int blocklengths6[structlen6];
	  //MPI_Datatype types6[structlen6];
	  //MPI_Aint displacements6[structlen6];

  			int dg = (l.qual)->level_comm.size();
			//cout << "Rank: " << rank << send_p << "\t" << dg << endl;
			MPI_Send(&dg, 1, MPI_INT, 0, 1,  MPI_COMM_WORLD);
			//MPI_Isend(&dg, 1, MPI_INT, send_p, 1, MPI_COMM_WORLD, &request);

            myobject_nn.x = new int[dg];

            //myobject_nn.y = qp.first;
            myobject_nn.z = qp.second;
            //changed to part-weight now
            myobject_nn.w = (l.qual)->g.part_weight;
            //myobject_nn.w = (l.qual)->g.total_weight;

            // others total, 0 part: smometimes in range :correct
            //others part, 0 part:  1st value always within range -1
            //others part, 0 total: very few iteration
            // others total, 0 total :  community as the 1st one, smometimes in range :correct

            myobject_nn.y = in_l2;
            //myobject_nn.z = tot_l2;

            // where are the components relative to the structure?

            blocklengths5[0] = dg; types5[0] = MPI_INT;
            displacements5[0] = (size_t)&(myobject_nn.x[0]) - (size_t)&myobject_nn;
            blocklengths5[1] = 1; types5[1] = MPI_LONG_DOUBLE;
            displacements5[1] = (size_t)&(myobject_nn.y) - (size_t)&myobject_nn;
            blocklengths5[2] = 1; types5[2] = MPI_LONG_DOUBLE;
            displacements5[2] = (size_t)&(myobject_nn.z) - (size_t)&myobject_nn;
            blocklengths5[3] = 1; types5[3] = MPI_LONG_DOUBLE;
            displacements5[3] = (size_t)&(myobject_nn.w) - (size_t)&myobject_nn;

            MPI_Type_create_struct(structlen5,blocklengths5,displacements5,types5,&newstructuretype5);
            MPI_Type_commit(&newstructuretype5);
            {
              MPI_Aint typesize5;
              MPI_Type_extent(newstructuretype5,&typesize5);
              //if (myrank==0)
              //printf("Type extent: %d bytes\n",typesize);
            }
                for(int i=0;i<dg;i++)
                {
                    myobject_nn.x[i]=(l.qual)->level_comm[i];
                    //cout << " n2c: " << myobject_nn.x[i] <<  " \t " << endl;

                }
                //cout << endl << "Quality: " << myobject_nn.y - myobject_nn.z << endl;

            MPI_Send(&myobject_nn,1,newstructuretype5,0,0,MPI_COMM_WORLD);
            MPI_Type_free(&newstructuretype5);
            //sending n2c array to processor 0
            int n2cc [(l.qual)->size];
            for(int i=0; i< (l.qual)->size; i++ )
            {
                int node=i+(l.qual)->g.start;
                n2cc[i]=(l.qual)->n2c[node];
            }
            MPI_Send(n2cc, (l.qual)->size, MPI_INT, 0, 2, MPI_COMM_WORLD);
            delete[] myobject_nn.x;
            long long end=current_timestamp();
            if(rank==1)cerr << "Total duration in Send: (sending unique community to 0) " << (end-start) << " milisec" << endl;

    }

    else
    {
		long double qq=0;
		//qq=(c.qual)->quality();
		//long double in1_l=qp.first;
		long double in1_l=in_l2;
		long double tot1_l=qp.second;
		long double ww1=(l.qual)->g.part_weight;
		//long double ww1=(l.qual)->g.total_weight;

		for(int i=0;i<world_size-1;i++)
		{
                int dg=0;
                MPI_Recv(&dg, 1, MPI_INT,  MPI_ANY_SOURCE, 1, MPI_COMM_WORLD, &status);
                int rec_p=status.MPI_SOURCE;
                //cout << "Process " << rank << " Received: " << dg << " from pro " << rec_p << endl;
                myobject_nn.x = new int[dg];

                // where are the components relative to the structure?

                blocklengths5[0] = dg; types5[0] = MPI_INT;
                displacements5[0] = (size_t)&(myobject_nn.x[0]) - (size_t)&myobject_nn;
                blocklengths5[1] = 1; types5[1] = MPI_LONG_DOUBLE;
                displacements5[1] = (size_t)&(myobject_nn .y) - (size_t)&myobject_nn;
                blocklengths5[2] = 1; types5[2] = MPI_LONG_DOUBLE;
                displacements5[2] = (size_t)&(myobject_nn.z) - (size_t)&myobject_nn;
                blocklengths5[3] = 1; types5[3] = MPI_LONG_DOUBLE;
                displacements5[3] = (size_t)&(myobject_nn.w) - (size_t)&myobject_nn;

                MPI_Type_create_struct(structlen5,blocklengths5,displacements5,types5,&newstructuretype5);
                MPI_Type_commit(&newstructuretype5);
                {
                  MPI_Aint typesize5;
                  MPI_Type_extent(newstructuretype5,&typesize5);
                  //if (myrank==0)
                  //printf("Type extent: %d bytes\n",typesize);
                }

                MPI_Recv(&myobject_nn,1,newstructuretype5,rec_p,0,MPI_COMM_WORLD, MPI_STATUS_IGNORE);
                MPI_Type_free(&newstructuretype5);
                //cout << "After Receive myobject elements: " << endl;
                for(int i=0;i<dg;i++)
                {
                    //
                    int comm = myobject_nn.x[i];
                    //cout << " n2c: " << myobject_nn.x[i] <<  " \t " << endl;

                    if((l.qual)->level_comm.size()==0){(l.qual)->level_comm.push_back(comm);}
                    if(std::find((l.qual)->level_comm.begin(), (l.qual)->level_comm.end(), comm) != (l.qual)->level_comm.end()) {
                                    /* v contains x */
                                    // do nothing
                    } else {
                        /* v does not contain x */
                        (l.qual)->level_comm.push_back(comm);
                    }

                }
                //cout << endl << "Rank: " << rank << " Quality: " << myobject_nn.y - myobject_nn.z << endl;
                in1_l += myobject_nn.y;
                tot1_l += myobject_nn.z;
                ww1+= myobject_nn.w;

                delete [] myobject_nn.x;

                // Receiving n2c array from all
                MPI_Probe(MPI_ANY_SOURCE, 2, MPI_COMM_WORLD, &status);
                rec_p=status.MPI_SOURCE;
                int number_amount;
                    // When probe returns, the status object has the size and other
                    // attributes of the incoming message. Get the message size
                    MPI_Get_count(&status, MPI_INT, &number_amount);

                    // Allocate a buffer to hold the incoming numbers
                    int* number_buf = (int*)malloc(sizeof(int) * number_amount);

                    // Now receive the message with the allocated buffer
                    MPI_Recv(number_buf, number_amount, MPI_INT, rec_p, 2, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

                //cout << "n2c elements: " << endl;
                for(int i=0;i< number_amount; i++){
                    int j=i+(l.qual)->node_process_new[rec_p];
                    (l.qual)->n2c[j]=number_buf[i];
                    //cout << "node: " << j << " comm: " << (c.qual)->n2c[j] << " \t" ;
                }
                //cout << endl;

                free(number_buf);



		}

		long long end=current_timestamp();
		cerr << "Total duration in Receive: (gathering updated communities from all): " << (end-start) << " milisec" << endl;
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>..

		//display_community (c, n2cfile);




//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>.

		sort ((l.qual)->level_comm.begin(), (l.qual)->level_comm.end());
		cout << (l.qual)->level_comm.size() ;
        //qq = (in1_l/ww1) - ((tot1_l/ww1)*(tot1_l/ww1));
		qq = (in1_l) - ((tot1_l/ww1)*(tot1_l/ww1));
		//long double temp = myobject_nn.y - myobject_nn.z;
		//qq += temp;
		//cout << endl << "Level 1 Quality: " << qq << endl ;
		cout << ":"  << qq << endl ;
//????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????
	/*	cout << (c.qual)->level_comm.size() << " (c.qual)->level_comm elements:" << endl;
		for(vector<int>::const_iterator z2 = (c.qual)->level_comm.begin(); z2 != (c.qual)->level_comm.end(); ++z2)
		{
			cout << *z2 << "\t";
		}
		cout << endl;*/
//??????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????????//



    }








}


void display_community (Louvain l, char * filen2c)
{
	ofstream foutput;
  	foutput.open(filen2c ,fstream::out | fstream::app);
	for (int i=0 ; i<(l.qual)->g.g_nb_nodes ; i++)
	{
    		foutput << i << " " << (l.qual)->n2c[i] << endl;
	}
	foutput.close();

}




Graph partition2graph_binary_pm( Louvain l, Graph g1, char * filen2c) {
  ofstream foutput;
  foutput.open(filen2c ,fstream::out | fstream::app);
  // Renumber communities
  vector<int> renumber((l.qual)->g_size, -1);
  for (int node=0 ; node < (l.qual)->g_size ; node++){
    renumber[(l.qual)->n2c[node]]++;
    //cerr << "Within Louvain::partition2graph_binary() renumber[qual->n2c["<<node<<"]] "<< renumber[qual->n2c[node]] << "\tn2c[" << node << "] " << qual->n2c[node] << endl;
  }
  int last=0;
  for (int i=0 ; i < (l.qual)->g_size ; i++) {
    if (renumber[i]!=-1)
      renumber[i] = last++;
  }

  for (int i=0 ; i <(l.qual)->g_size; i++)
  {
    foutput << i << " " << renumber[(l.qual)->n2c[i]] << endl;
  }
  foutput.close();
  // Compute communities
  vector<vector<int> > comm_nodes(last);
  vector<int> comm_weight(last, 0);

  for (int node = 0 ; node < g1.nb_nodes ; node++) {
    comm_nodes[renumber[(l.qual)->n2c[node]]].push_back(node);
    comm_weight[renumber[(l.qual)->n2c[node]]] += g1.nodes_w[node];
    //cerr << "Within Louvain::partition2graph_binary() renumber[qual->n2c["<<node<<"]] "<< renumber[qual->n2c[node]] << "\tn2c[" << node << "] " << qual->n2c[node] << endl;
  }

  // Compute weighted graph
  Graph g2;
  int nbc = comm_nodes.size();

  g2.nb_nodes = comm_nodes.size();
  g2.g_nb_nodes = comm_nodes.size();
  g2.start = 0;
  g2.degrees.resize(nbc);
  g2.nodes_w.resize(nbc);

  for (int comm=0 ; comm<nbc ; comm++) {
    map<int,long double> m;
    map<int,long double>::iterator it;

    int size_c = comm_nodes[comm].size();

    g2.assign_weight(comm, comm_weight[comm]);

    for (int node=0 ; node<size_c ; node++) {
      pair<vector<int>::iterator, vector<long double>::iterator> p = g1.neighbors(comm_nodes[comm][node]);
      int deg = g1.nb_neighbors(comm_nodes[comm][node]);
      for (int i=0 ; i<deg ; i++) {
	int neigh = *(p.first+i);
	int neigh_comm = renumber[(l.qual)->n2c[neigh]];
	long double neigh_weight = (g1.weights.size()==0)?1.0L:*(p.second+i);

	it = m.find(neigh_comm);
	if (it==m.end())
	  m.insert(make_pair(neigh_comm, neigh_weight));
	else
	  it->second += neigh_weight;
      }
    }

    g2.degrees[comm] = (comm==0)?m.size():g2.degrees[comm-1]+m.size();
    g2.nb_links += m.size();

    for (it = m.begin() ; it!=m.end() ; it++) {
      g2.total_weight += it->second;
      g2.links.push_back(it->first);
      g2.weights.push_back(it->second);
    }
  }
  comm_nodes.clear();
  comm_weight.clear();
  renumber.clear();

  return g2;
}


void final_compute(Louvain l)
{


    // Graph Constructor of full network (in sequential format), required for merging
		Graph g2(filename_full, filename_full_w, type, 1);
		//cout <<  "Network constructed " << endl ;
		Graph g3 = partition2graph_binary_pm( l, g2, n2cfile);
		//cout <<  "Merging Done " << endl ;
		g3.display_input(infile);
		// keep full network binary for next merging
		g3.display_binary(outfile_2, outfile_w);

	       // Graph g_test(outfile_2,outfile_w);	//For test only, not required
	        //cout << "Checking graph " << endl;
	        //g_test.display_input("l2-test.txt");
		//g_test.display();

        time(&time_end);
        end_time=current_timestamp();
 //       cerr << "Final duration: " << (time_end-time_begin) << " sec" << endl;
        cerr << "Final duration: " << (end_time-start_time) << " milisec" << endl;





}



vector<int> remove_elem(vector<int>v, int val)
{
    for( std::vector<int>::iterator iter = v.begin(); iter != v.end(); ++iter )
    {
        if( *iter == val )
        {
            v.erase( iter );
            break;
        }
    }
    return v;
}


int main(int argc, char **argv) {
  srand(time(NULL)+getpid());


  //processor_name =new char[MPI_MAX_PROCESSOR_NAME];
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  //omp_set_num_threads(num_threads);
  parse_args(argc, argv);

  //init();
  time(&time_begin);
  start_time=current_timestamp();
  stringstream rankS;
  rankS << rank;
  //rankS << 1;
  string s(filename);

  string so(filename);
  s += "-" + rankS.str() + ".bin";

  //s += "-" + "1" + ".bin";
  so += "_out-" + rankS.str() + ".tree";
  unsigned short nb_calls = 0;


  char* tmp = new char[s.length() + 1];
  strcpy(tmp, s.c_str());



  outfile = new char[so.length() + 1];
  strcpy(outfile, so.c_str());
  if(type == WEIGHTED)
  {
	  string ss(filename_w);
	  ss += "-" + rankS.str() + ".bin";
	  filename_w =new char[ss.length() + 1];
	  strcpy(filename_w, ss.c_str());
  }
  if (verbose)
    display_time("Begin");
  //Graph g(filename, filename_w, type);
  Graph g(world_size, tmp, filename_w, type);
  delete[] tmp;


  init_quality(&g, nb_calls);
 // g.display(outfile);
  delete [] outfile;
  //g.display();
  nb_calls++;

  if (verbose)
    cerr << endl << "Computation of communities with the " << q->name << " quality function" << endl << endl;



  //Louvain c(-1, precision, q);
  Louvain c(-1, precision, q,rank);
  //if (filename_part!=NULL)c.init_partition(filename_part);


//################################################################
// Exchanging starting node number with other processors
	exchange_starting_node(c);


// #############################################################################################

// getting neighbor list of neighbors from other processors
	gather_neighbor_info(c);


  MPI_Barrier( MPI_COMM_WORLD ) ;


//##############################################################################

    compute_community(c);




//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	MPI_Barrier( MPI_COMM_WORLD ) ;
	send_list_update(c);
// Updating receive_list size
	receive_list_update(c);

	MPI_Barrier( MPI_COMM_WORLD ) ;
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
//	exchange_updated_community(c);
	exchange_updated_community_2(c);


//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  //new_qual = (c.qual)->quality();
  time(&time_end);
  if (verbose) {
    display_time("End");
    cerr << "Total duration: " << (time_end-time_begin) << " sec" << endl;
  }
  //cout << "Rank: " << rank << " Quality: " << new_qual << endl;
  //cout <<  new_qual << endl;


//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// Display n2c

MPI_Barrier( MPI_COMM_WORLD ) ; // gives wrong output and hangs without this barrier
/*
   for (int i=(c.qual)->g.start ; i<(c.qual)->g.start+(c.qual)->g.nb_nodes ; i++) {
    cout << i << " " << (c.qual)->n2c[i] << endl;usleep(200);
}
MPI_Barrier( MPI_COMM_WORLD ) ;
*/


//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


/*cout << "@@@@@@@@@@@@@@@@@ in process " << rank << "@@@@@@@@" << endl;
for (int i=0 ; i<(c.qual)->g.g_nb_nodes ; i++) {
    cout << i << "\t" << (c.qual)->n2c[i] << endl;
}
*/
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	//updating comm_rcv array for solving community duality
	resolve_duality(c);

//**************************************************************************************************************************
//Testing arrays for communication
/*
	cout << node_list.size() << " node_list elements:" << endl;
	for(vector<int>::const_iterator z2 = node_list.begin(); z2 != node_list.end(); ++z2)
	{
		cout << *z2 << "\t";
	}
	cout << endl ;
		cout << comm_list.size() << " comm_list elements:" << endl;
		for(vector<int>::const_iterator z2 = comm_list.begin(); z2 != comm_list.end(); ++z2)
		{
			cout << *z2 << "\t";
		}
		cout << endl ;
*/

//********************************************************************************************************************

	//
	//cout <<  " comm_rcv_2 elements:" << endl;
// updating send_list array
    send_list2_update();

// Updating receive_list size
	receive_list_update();
	//cout << "rank: " << rank << "r_l_size: " << r_l_size << " rvec size: " << r_l.size() << " s_l_size: " << s_l_size << " svec size: " << s_l.size() << endl;
	MPI_Barrier( MPI_COMM_WORLD ) ;
//*******************************************************************************************************************
//Testing arrays for communication
/*
	cout << s_l.size() << " s_l elements:" << endl;
	for(vector<int>::const_iterator z2 = s_l.begin(); z2 != s_l.end(); ++z2)
	{
		cout << *z2 << "\t";
	}
	cout << endl ;
	cout << r_l.size() << " r_l elements:" << endl;
	for(vector<int>::const_iterator z2 = r_l.begin(); z2 != r_l.end(); ++z2)
	{
		cout << *z2 << "\t";
	}
	cout << endl ;
*/
//****************************************************************************************************************
// &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  exchange_duality_resolved_community(c);

// Test Print
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// Display n2c

	MPI_Barrier( MPI_COMM_WORLD ) ;
/*   for (int i=(c.qual)->g.start ; i<(c.qual)->g.start+(c.qual)->g.nb_nodes ; i++) {
    cout << i << " " << (c.qual)->n2c[i] << endl;usleep(200);
   }
	MPI_Barrier( MPI_COMM_WORLD ) ;
*/

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>



//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// after line p in Pseudocode
    // find unique communities in each processor
    find_unique_communities(c);
    //  merged all communities in processor 0 and renumbered them for next level and computed modularity for whole graph and
    renumber_communities(c);


// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Merging in node 0

    if(rank==0)final_compute(c);    // generate graph for next level







//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  delete q;




  MPI_Finalize();

}
