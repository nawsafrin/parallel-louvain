from __future__ import print_function
import __future__
import numpy
import networkx as nx
import matplotlib.pyplot as plt
arr = []
#inp = open ("X_l2.txt","r")
#inp = open ("X_l1.txt","r")
#inp = open ("g_X_0.txt","r")
#inp = open ("X-ex-t-l1.txt","r")
#inp = open ("gt-l-X4.txt","r")
inp = open ("gt-s-X-2.txt","r")
#read line into array 
for line in inp.readlines():
    # add a new sublist
    arr.append([])
    # loop over the elemets, split by whitespace
    for i in line.split():
        # convert to integer and append to the last
        # element of the list
        arr[-1].append(int(i))

#for i in range(len(arr)):
#    for j in range(len(arr[i])):
#        print(arr[i][j], end=' ')
#    print()
A=numpy.matrix(arr)
G=nx.from_numpy_matrix(A)
pos=nx.spring_layout(G) # positions for all nodes   
nx.draw(G,pos, with_labels = True)
plt.show()

